/*
Navicat MySQL Data Transfer

Source Server         : AnandaMahdar
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : zeeals_portal

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-05-05 09:34:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ads
-- ----------------------------
DROP TABLE IF EXISTS `ads`;
CREATE TABLE `ads` (
  `id_ads` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ads` varchar(200) DEFAULT NULL,
  `tipe_ads` varchar(200) DEFAULT NULL,
  `harga_ads` int(100) DEFAULT NULL,
  `description_ads` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id_ads`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ads
-- ----------------------------

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `nama_category` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(10) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of category
-- ----------------------------

-- ----------------------------
-- Table structure for foto_preview_product
-- ----------------------------
DROP TABLE IF EXISTS `foto_preview_product`;
CREATE TABLE `foto_preview_product` (
  `foto_id` int(11) NOT NULL AUTO_INCREMENT,
  `foto_url` text NOT NULL,
  `label` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(10) DEFAULT NULL,
  PRIMARY KEY (`foto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of foto_preview_product
-- ----------------------------

-- ----------------------------
-- Table structure for kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE `kecamatan` (
  `kecamatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kecamatan_nama` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`kecamatan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kecamatan
-- ----------------------------

-- ----------------------------
-- Table structure for kelurahan
-- ----------------------------
DROP TABLE IF EXISTS `kelurahan`;
CREATE TABLE `kelurahan` (
  `kelurahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelurahan_nama` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`kelurahan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelurahan
-- ----------------------------

-- ----------------------------
-- Table structure for kotakab
-- ----------------------------
DROP TABLE IF EXISTS `kotakab`;
CREATE TABLE `kotakab` (
  `kotakab_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kotakab` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(10) DEFAULT NULL,
  PRIMARY KEY (`kotakab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kotakab
-- ----------------------------

-- ----------------------------
-- Table structure for location
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `onlineshop_id` int(11) DEFAULT NULL,
  `alamat` text,
  `label` varchar(255) DEFAULT NULL,
  `longitude` varchar(100) DEFAULT NULL,
  `latitude` varchar(100) DEFAULT NULL,
  `notelp` int(13) DEFAULT NULL,
  `fototoko` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  `kotakab_id` int(11) DEFAULT NULL,
  `kelurahan_id` int(11) DEFAULT NULL,
  `kecamatan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`location_id`),
  KEY `onlineshop_id` (`onlineshop_id`),
  KEY `location_kotakab` (`kotakab_id`),
  KEY `location_kelurahan` (`kelurahan_id`),
  KEY `location_kecamatan` (`kecamatan_id`),
  CONSTRAINT `location_ibfk_1` FOREIGN KEY (`onlineshop_id`) REFERENCES `onlineshop` (`onlineshop_id`),
  CONSTRAINT `location_kecamatan` FOREIGN KEY (`kecamatan_id`) REFERENCES `kecamatan` (`kecamatan_id`),
  CONSTRAINT `location_kelurahan` FOREIGN KEY (`kelurahan_id`) REFERENCES `kelurahan` (`kelurahan_id`),
  CONSTRAINT `location_kotakab` FOREIGN KEY (`kotakab_id`) REFERENCES `kotakab` (`kotakab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of location
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `MENU_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MEN_MENU_ID` int(11) DEFAULT NULL,
  `MENU_NAME` varchar(50) NOT NULL,
  `MENU_LINK` varchar(150) NOT NULL,
  `MENU_STATUS` tinyint(1) NOT NULL,
  `MENU_ISPARENT` tinyint(1) NOT NULL,
  `MENU_ORDER` int(11) DEFAULT NULL,
  PRIMARY KEY (`MENU_ID`),
  KEY `FK_MENU_PARENT` (`MEN_MENU_ID`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`MEN_MENU_ID`) REFERENCES `menu` (`MENU_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', null, 'Informasi Website', 'website_info', '1', '1', '1');
INSERT INTO `menu` VALUES ('16', null, 'Pengaturan', '#', '1', '1', '16');
INSERT INTO `menu` VALUES ('17', '16', 'Pengguna', 'users', '1', '0', '17');
INSERT INTO `menu` VALUES ('18', '16', 'Menu', 'menu', '1', '0', '18');
INSERT INTO `menu` VALUES ('19', '16', 'Role', 'role', '1', '0', '19');
INSERT INTO `menu` VALUES ('20', null, 'Log ', 'activity_log', '0', '1', '20');
INSERT INTO `menu` VALUES ('21', null, 'Pesan', 'messages', '1', '1', '21');
INSERT INTO `menu` VALUES ('22', null, 'Pengikut', 'subscribers', '1', '1', '22');

-- ----------------------------
-- Table structure for menu_role
-- ----------------------------
DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE `menu_role` (
  `ROLE_ID` int(11) NOT NULL,
  `MENU_ID` int(11) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`MENU_ID`),
  KEY `FK_R_10` (`MENU_ID`),
  CONSTRAINT `menu_role_ibfk_1` FOREIGN KEY (`MENU_ID`) REFERENCES `menu` (`MENU_ID`),
  CONSTRAINT `menu_role_ibfk_2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu_role
-- ----------------------------
INSERT INTO `menu_role` VALUES ('1', '1');
INSERT INTO `menu_role` VALUES ('1', '16');
INSERT INTO `menu_role` VALUES ('1', '17');
INSERT INTO `menu_role` VALUES ('1', '18');
INSERT INTO `menu_role` VALUES ('1', '19');
INSERT INTO `menu_role` VALUES ('1', '21');
INSERT INTO `menu_role` VALUES ('1', '22');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `MESSAGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MESSAGE_NAME` varchar(50) NOT NULL,
  `MESSAGE_EMAIL` varchar(100) NOT NULL,
  `MESSAGE_TITLE` varchar(255) DEFAULT NULL,
  `MESSAGE_CONTENT` text NOT NULL,
  `MESSAGE_DATE` datetime NOT NULL,
  `MESSAGE_STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`MESSAGE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', 'septi', 'septi@gkfjdk.com', 'testing subject', 'pesan 1', '2015-08-19 13:16:30', '1');
INSERT INTO `messages` VALUES ('2', 'testi', 'fsdf@fjdkf.css', 'dfsjksdjfsdf dfssd', 'dsfsdfsdfsd', '2015-08-19 13:19:02', '1');
INSERT INTO `messages` VALUES ('3', 'septi', 'septi@septi.com', 'testing saja', 'pesan saya', '2015-08-21 10:07:36', '1');

-- ----------------------------
-- Table structure for onlineshop
-- ----------------------------
DROP TABLE IF EXISTS `onlineshop`;
CREATE TABLE `onlineshop` (
  `onlineshop_id` int(11) NOT NULL AUTO_INCREMENT,
  `onlineshop_name` varchar(200) DEFAULT NULL,
  `onlineshop_logo` varchar(500) DEFAULT NULL,
  `onlineshop_username` varchar(10) DEFAULT NULL,
  `onlineshop_password` varchar(500) DEFAULT NULL,
  `onlineshop_owner` varchar(250) DEFAULT NULL,
  `onlineshop_description` text,
  `onlineshop_hp` varchar(12) DEFAULT NULL,
  `onlineshop_bbm` varchar(20) DEFAULT NULL,
  `onlineshop_email` varchar(50) DEFAULT NULL,
  `onlineshop_status` tinyint(2) DEFAULT NULL,
  `onlineshop_rating` int(11) DEFAULT NULL,
  `onlineshop_link` varchar(500) DEFAULT NULL,
  `onlineshop_click` int(11) DEFAULT NULL,
  `id_ads` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `foto_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`onlineshop_id`),
  KEY `onlineshop_category` (`category_id`),
  KEY `onlineshop_ads` (`id_ads`),
  KEY `onlineshop_foto` (`foto_id`),
  CONSTRAINT `onlineshop_ads` FOREIGN KEY (`id_ads`) REFERENCES `ads` (`id_ads`),
  CONSTRAINT `onlineshop_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`),
  CONSTRAINT `onlineshop_foto` FOREIGN KEY (`foto_id`) REFERENCES `foto_preview_product` (`foto_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of onlineshop
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_NAME` varchar(50) NOT NULL,
  `ROLE_STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Admin', '1');
INSERT INTO `role` VALUES ('2', 'Penulis Berita', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` int(11) NOT NULL,
  `USER_FULLNAME` varchar(100) NOT NULL,
  `USER_USERNAME` varchar(50) NOT NULL,
  `USER_EMAIL` varchar(100) NOT NULL,
  `USER_PASSWORD` varchar(255) NOT NULL,
  `USER_PHOTO` varchar(255) DEFAULT NULL,
  `USER_REGISTERED_ON` datetime NOT NULL,
  `USER_STATUS` tinyint(1) NOT NULL,
  PRIMARY KEY (`USER_ID`),
  KEY `AK_UQ_EMAIL` (`USER_EMAIL`),
  KEY `AK_UQ_USERNAME` (`USER_USERNAME`),
  KEY `FK_R_8` (`ROLE_ID`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'Administrator', 'admin', 'admin@psma.com', '21232f297a57a5a743894a0e4a801fc3', null, '0000-00-00 00:00:00', '1');
INSERT INTO `users` VALUES ('2', '2', 'septi', 'septi', 'septi@septi.com', 'd58d8a16aa666d48fbcc30bd3217fb17', null, '2015-01-01 00:00:00', '1');
INSERT INTO `users` VALUES ('3', '1', 'Cobalah', 'coba', 'coba@gmail.com', '781c5303581ec465792ee32f6dd0c84c', null, '2017-05-03 08:05:33', '1');
INSERT INTO `users` VALUES ('4', '2', 'Ananda Mahdar', 'ananda', 'anadamahdaralsdjl@yahodkjf.co.fd.id', '19291968f92183c01cc0655ff644594b', null, '2017-05-05 03:05:06', '1');
