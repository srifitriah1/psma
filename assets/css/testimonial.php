	<!-- BEGIN .page-header -->
	<div class="page-header clearfix">
		
		<div class="page-header-inner clearfix">
		
		<div class="page-title">	
			<h2>Testimonial</h2>
			<div class="page-title-block"></div>
		</div>
		
		<div class="breadcrumbs">
			<p>
            	<a href="<?php echo base_url(); ?>">Home</a> &#187;
                Testimonial
            </p>
		</div>
		
		</div>
		
	<!-- END .page-header -->
	</div>
	
	<!-- BEGIN .content-wrapper -->
	<div class="content-wrapper page-content-wrapper clearfix">
		
		<!-- BEGIN .main-content -->
		<div class="main-content page-content">
			
			<!-- BEGIN .inner-content-wrapper -->
			<div class="inner-content-wrapper">
			
            	<?php
            	foreach($testimonial as $testi){
            		?>
					<div class="teacher-entry clearfix">					
						<div class="teacher-image">
							<img src="<?php echo base_url().$testi->testimonial_photo?>" alt="<?php echo $testi->testimonial_name; ?>" />
						</div>					
						<div class="teacher-content">							
							<div class="title1 clearfix">
								<h4><?php echo $testi->testimonial_name; ?></h4>
	                            <br>
							</div>						
							<blockquote><p><?php echo $testi->testimonial_content; ?></p></blockquote>											
						</div>				
					</div>
            		<?php
            	}
            	?>
                           
			<!-- END .inner-content-wrapper -->
			</div>
			
		<!-- END .main-content -->
		</div>
		
		<!-- BEGIN .sidebar-right -->
		<div class="sidebar-right page-content">
			
			<?php $this->load->view('sub_layanan_lainnya'); ?>
			
			<?php $this->load->view('sub_galeri_foto'); ?>
			
			<?php $this->load->view('sub_download'); ?>

		
		<!-- END .sidebar-right -->
		</div>
	
	<!-- END .content-wrapper -->
	</div>