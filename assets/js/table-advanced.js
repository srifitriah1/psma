var TableAdvanced = function () {

    // var initTable1 = function() {

    //     /* Formatting function for row details */
    //     function fnFormatDetails ( oTable, nTr )
    //     {
    //         var aData = oTable.fnGetData( nTr );
    //         var sOut = '<table>';
    //         sOut += '<tr><td>Platform(s):</td><td>'+aData[2]+'</td></tr>';
    //         sOut += '<tr><td>Engine version:</td><td>'+aData[3]+'</td></tr>';
    //         sOut += '<tr><td>CSS grade:</td><td>'+aData[4]+'</td></tr>';
    //         sOut += '<tr><td>Others:</td><td>Could provide a link here</td></tr>';
    //         sOut += '</table>';
             
    //         return sOut;
    //     }

    //     /*
    //      * Insert a 'details' column to the table
    //      */
    //     var nCloneTh = document.createElement( 'th' );
    //     var nCloneTd = document.createElement( 'td' );
    //     nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';
         
    //     $('#sample_1 thead tr').each( function () {
    //         this.insertBefore( nCloneTh, this.childNodes[0] );
    //     } );
         
    //     $('#sample_1 tbody tr').each( function () {
    //         this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
    //     } );
         
    //     /*
    //      * Initialize DataTables, with no sorting on the 'details' column
    //      */
    //     var oTable = $('#sample_1').dataTable( {
    //         "aoColumnDefs": [
    //             {"bSortable": false, "aTargets": [ 0 ] }
    //         ],
    //         "aaSorting": [[1, 'asc']],
    //          "aLengthMenu": [
    //             [5, 15, 20, -1],
    //             [5, 15, 20, "All"] // change per page values here
    //         ],
    //         // set the initial value
    //         "iDisplayLength": 10,
    //     });

    //     jQuery('#sample_1_wrapper .dataTables_filter input').addClass("form-control input-small"); // modify table search input
    //     jQuery('#sample_1_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
    //     jQuery('#sample_1_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
         
    //     /* Add event listener for opening and closing details
    //      * Note that the indicator for showing which row is open is not controlled by DataTables,
    //      * rather it is done here
    //      */
    //     $('#sample_1').on('click', ' tbody td .row-details', function () {
    //         var nTr = $(this).parents('tr')[0];
    //         if ( oTable.fnIsOpen(nTr) )
    //         {
    //             /* This row is already open - close it */
    //             $(this).addClass("row-details-close").removeClass("row-details-open");
    //             oTable.fnClose( nTr );
    //         }
    //         else
    //         {
    //             /* Open this row */                
    //             $(this).addClass("row-details-open").removeClass("row-details-close");
    //             oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
    //         }
    //     });
    // }

     var initTable2 = function() {
        var oTable = $('#table_data').dataTable( {           
            "bPaginate": false,
            "bSort": false,
            "bSearchable": false,
            "bFilter": false,
            "bInfo": false
        });


        $('#table_data_column_toggler input[type="checkbox"]').change(function(){
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });

        jQuery('#table_data .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        var par = $('#table_data').parents('.portlet-body');
                        $(par).find('#deleteall').attr('disabled', false);
                    } else {
                        $(this).attr("checked", false);
                        var par = $('#table_data').parents('.portlet-body');
                        $(par).find('#deleteall').attr('disabled', true);
                    }
                    $(this).parents('tr').toggleClass("active");
                });
                jQuery.uniform.update(set);

            });

         jQuery('#table_search .group-checkable').change(function () {
                var set = jQuery(this).attr("data-set");
                var checked = jQuery(this).is(":checked");
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                        var par = $('#table_search').parents('.portlet-body');
                        $(par).find('#deleteall').attr('disabled', false);
                    } else {
                        $(this).attr("checked", false);
                        var par = $('#table_search').parents('.portlet-body');
                        $(par).find('#deleteall').attr('disabled', true);
                    }
                    $(this).parents('tr').toggleClass("active");
                });
                jQuery.uniform.update(set);

            });

         jQuery('#table_data tbody tr .checkboxes').change(function(){
                 $(this).parents('tr').toggleClass("active");
                 var checked = jQuery(this).is(":checked");
                 var value2 = [];
                 $('#table_data tbody tr .checkboxes:checked').each(function(i){
                    value2[i] = $(this).val();
                 });
                 if (checked) {
                    var par = $('#table_data').parents('.portlet-body');
                    $(par).find('#deleteall').attr('disabled', false);   
                 }else{
                    if (value2 == 0) {
                        var par = $('#table_data').parents('.portlet-body');
                        $(par).find('#deleteall').attr('disabled', true);   
                    }
                 };
            });

         jQuery('#table_search tbody tr .checkboxes').change(function(){
                 $(this).parents('tr').toggleClass("active");
                 var checked = jQuery(this).is(":checked");
                 var value = [];
                 $('#table_search tbody tr .checkboxes:checked').each(function(i){
                    value[i] = $(this).val();
                 });
                 if (checked) {
                    var par = $('#table_search').parents('.portlet-body');
                    $(par).find('#deleteall').attr('disabled', false);   
                 }else{
                    if(value == 0){
                        var par = $('#table_search').parents('.portlet-body');
                        $(par).find('#deleteall').attr('disabled', true);   
                    }
                 };
                 
            });

    }

    return {

        //main function to initiate the module
        init: function () {
            
            if (!jQuery().dataTable) {
                return;
            }

            // initTable1();
            initTable2();
        }

    };

}();