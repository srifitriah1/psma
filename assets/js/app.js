/**
Core script to handle the entire theme and core functions
**/
var App = function () {

    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

    var sidebarWidth = 225;
    var sidebarCollapsedWidth = 35;

    var responsiveHandlers = [];

    // theme layout color set
    var layoutColorCodes = {
        'blue': '#4b8df8',
        'red': '#e02222',
        'green': '#35aa47',
        'purple': '#852b99',
        'grey': '#555555',
        'light-grey': '#fafafa',
        'yellow': '#ffb848'
    };

    // To get the correct viewport width based on  http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
    var _getViewPort = function () {
        var e = window, a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return {
            width: e[a + 'Width'],
            height: e[a + 'Height']
        }
    }
	
	var handleAddButton = function () {
        var check_input = $('.file-name input').length;       
        $("a.add-input").live('click', function(){
            var name = '';
            var name_text = '';
            var placehol = '';
            var data_name = $(this).closest('table').find('.input-file').data('name');
            // console.log(data_name);
            if(data_name === null || typeof data_name === 'undefined'){
                var nameForFile = $(this).parents('.form-group').find('.control-label').text().toLowerCase();
                if(nameForFile.indexOf('audio') >= 0){
                    name = 'nama_audio';
                    name_text = 'nama_file_audio';
                    placehol = 'Nama Audio';
                }else if(nameForFile.indexOf('video') >= 0){
                    name = 'nama_video';
                    name_text = 'nama_file_video';
                    placehol = 'Nama Video';
                }else{
                    name = 'nama_foto';
                    name_text = 'nama_file_foto';
                    placehol = 'Nama Foto';
                }
            }else{
                name = data_name;
            }
            
            var input = '<div class="fileupload fileupload-new" data-provides="fileupload">'+
                            '<div class="input-group">'+
                                '<span class="input-group-btn">'+
                                    '<span class="uneditable-input">'+
                                        '<i class="fa fa-file fileupload-exists"></i>'+
                                        '<span class="fileupload-preview"></span>'+
                                    '</span>'+
                                '</span>'+
                                '<span class="btn default btn-file">'+
                                    '<span class="fileupload-new"><i class="fa fa-paperclip"></i> Pilih File</span>'+
                                    '<span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah File</span>'+
                                    '<input type="file" class="default" name="'+name+'[]" placeholder="foto"/>'+
                                '</span>'+
                                '<a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Hapus</a>'+
                            '</div>'+
                        '</div>';
            var button = "<a style='margin-top: -9px;' class='btn red remove_selected'><span class='fa fa-minus'></span></a>";
            var file = '';
            // console.log(file_name);
            if(check_input > 0){                
                var file_name = '<input style="margin-top: -9px; margin-left: 5px;" class="form-control " name="'+name_text+'[]" placeholder="'+placehol+'">';
                file = '<td>'+file_name+'</td>';
            }
            $(this).parents('table').append('<tr><td>'+button+'</td><td>'+input+'</td>'+file+'</tr>');
        });
        $('a.add-input-sec').live('click', function(){
            var button = "<a class='btn red remove_selected_dl'><span class='fa fa-minus'></span></a>";
            var input = $(this).parents('dl').find(".input-file-sec").html();
            var input2 = $(this).parents('dl').find(".input-file").html();
            $(this).parents('.media-input').append("<dl class='dl-horizontal'><dt>"+button+"</dt><dd>"+input+"</dd><dd>"+input2+"</dd></dl>");
        });
        $("a.remove_selected_dl").live('click', function(e){
            e.preventDefault();
            var row = $(this).parents('dl')[0];            
            row.remove();
        });
        $("a.remove_selected_block").live('click', function(e){
            e.preventDefault();
            var row = $(this).parents('.add-block')[0];            
            row.remove();
        });
        $('.input-file').on('click','.fileupload-exists', function(e){
            e.preventDefault();
            $(this).closest('tr').find('.file-name input.form-control').val('');
        })
    }

	
	var handleMaps = function (){
        var page = window.location.pathname;
        var markers = [];
        var polyCoordinates = '';
        var det_lat = $('input[name=address_latitude]').val();
        var det_lng = $('input[name=address_longitude]').val();
        if (page.indexOf('detail') >= 0) {
            $('#inputaddress').hide();
            $('input[name=longlat]').parents('.input-group').hide();
            if (det_lat.length !== 0 && det_lng !== 0) {
                google.maps.event.addDomListener(window, 'load', map_detail_init(det_lat, det_lng));
            }
        }else{
            if (det_lat.length !== 0 && det_lng.length !== 0) {
                var longitudelatitudelads = "("+det_lat+","+det_lng+")";
                $('input[name=longlat]').val(longitudelatitudelads);
            }
            google.maps.event.addDomListener(window, 'load', map_initialize(det_lat, det_lng));
        }

        function map_detail_init(lat, lng){
            $('#map_lokasi').css('width', '100%');
            $('#map_lokasi').css('height', '350px');
            if (lat.length === 0 && lng.length === 0){
                lat = -2.548926; lng = 118.0148634;
            }
            var latlng = new google.maps.LatLng(lat,lng);
            var myOptions = {
                zoom: 11,
                center: latlng,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
                scaleControl: false,
                draggable: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_lokasi"), myOptions);
            placeMarker(latlng, map);
        }

		
		
        function map_initialize(lat, lng){
            $('#map_lokasi').css('width', '100%');
            $('#map_lokasi').css('height', '350px');
            if (lat.length === 0 && lng.length === 0){
                lat = -2.548926; lng = 118.0148634;
            }
            var latlng = new google.maps.LatLng(lat, lng); // latitude is 29.392971,longitude: 79.454051
            var myOptions = {
                zoom: 5,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_lokasi"), myOptions);
            if (lat !== -2.548926 && lng.length !== 118.0148634){
                placeMarker(latlng, map);
            }
            clickable(map);
            searchable(map);
        }

        function clickable(map){
            google.maps.event.addListener(map, 'click', function(event) {
                polyCoordinates = event.latLng;
                $('input[name="longlat"]').val(polyCoordinates);
                $('input[name="address_latitude"]').val(polyCoordinates.lat());
                $('input[name="address_longitude"]').val(polyCoordinates.lng());
                console.log(polyCoordinates.lng());
                placeMarker(polyCoordinates, map);
            }); 
        }

        function searchable(map){
            var input = document.getElementById('inputaddress');
            var searchBox = new google.maps.places.SearchBox(/** @type {HTMLInputElement} */(input));
            google.maps.event.addListener(searchBox, 'places_changed', function() { 
                var places = searchBox.getPlaces();
                    polyCoordinates = places[0].geometry.location;
                    $('input[name="longlat"]').val(polyCoordinates);
                    $('input[name="address_latitude"]').val(polyCoordinates.lat());
                    $('input[name="address_longitude"]').val(polyCoordinates.lng());
                    map.setZoom(9);
                    map.setCenter(places[0].geometry.location);
                    placeMarker(places[0].geometry.location, map);
            });
        }   
    
        function placeMarker(location, map) {
            clearMarker();
            var marker = new google.maps.Marker({
                position: location, 
                map: map
            });
            map.setCenter(location);
            markers.push(marker);
        }

        function setMarker(map){
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        function clearMarker(){
            setMarker(null);
            polyCoordinates = null;
            markers = [];
        }

        $('.clearLat').live('click', function (e){
            e.preventDefault();
            clearMarker();
            $('input[name="longlat"]').val(polyCoordinates);
            $('input[name="lat"]').val(polyCoordinates);
            $('input[name="lng"]').val(polyCoordinates);
        });

    }
	
    // initializes main settings
    var handleInit = function () {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !! navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !! navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !! navigator.userAgent.match(/MSIE 10.0/);

        if (isIE10) {
            jQuery('html').addClass('ie10'); // detect IE10 version
        }
        
        if (isIE10 || isIE9 || isIE8) {
            jQuery('html').addClass('ie'); // detect IE10 version
        }

        /*
          Virtual keyboards:
          Also, note that if you're using inputs in your modal – iOS has a rendering bug which doesn't 
          update the position of fixed elements when the virtual keyboard is triggered  
        */
        var deviceAgent = navigator.userAgent.toLowerCase();
        if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
            $(document).on('focus', 'input, textarea', function () {
                $('.header').hide();
                $('.footer').hide();
            });
            $(document).on('blur', 'input, textarea', function () {
                $('.header').show();
                $('.footer').show();
            });
        }
    }

    var handleSidebarState = function () {
        // remove sidebar toggler if window width smaller than 992(for tablet and phone mode)
        var viewport = _getViewPort();
        if (viewport.width < 992) {
            $('body').removeClass("page-sidebar-closed");
        }
    }

    // runs callback functions set by App.addResponsiveHandler().
    var runResponsiveHandlers = function () {
        // reinitialize other subscribed elements
        for (var i in responsiveHandlers) {
            var each = responsiveHandlers[i];
            each.call();
        }
    }

    // reinitialize the laypot on window resize
    var handleResponsive = function () {
        handleSidebarState();
        handleSidebarAndContentHeight();
        handleFixedSidebar();
        runResponsiveHandlers();
    }

    // initialize the layout on page load
    var handleResponsiveOnInit = function () {
        handleSidebarState();
        handleSidebarAndContentHeight();
    }

    // handle the layout reinitialization on window resize
    var handleResponsiveOnResize = function () {
        var resize;
        if (isIE8) {
            var currheight;
            $(window).resize(function () {
                if (currheight == document.documentElement.clientHeight) {
                    return; //quite event since only body resized not window.
                }
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    handleResponsive();
                }, 50); // wait 50ms until window resize finishes.                
                currheight = document.documentElement.clientHeight; // store last body client height
            });
        } else {
            $(window).resize(function () {
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    handleResponsive();
                }, 50); // wait 50ms until window resize finishes.
            });
        }
    }

    //* BEGIN:CORE HANDLERS *//
    // this function handles responsive layout on screen size resize or mobile device rotate.

    // Set proper height for sidebar and content. The content and sidebar height must be synced always.
    var handleSidebarAndContentHeight = function () {
        var content = $('.page-content');
        var sidebar = $('.page-sidebar');
        var body = $('body');
        var height;

        if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
            var available_height = $(window).height() - $('.footer').outerHeight();
            if (content.height() < available_height) {
                content.attr('style', 'min-height:' + available_height + 'px !important');
            }
        } else {
            if (body.hasClass('page-sidebar-fixed')) {
                height = _calculateFixedSidebarViewportHeight();
            } else {
                height = sidebar.height() + 20;
            }
            if (height >= content.height()) {
                content.attr('style', 'min-height:' + height + 'px !important');
            }
        }
    }
	

    // Handle sidebar menu
    var handleSidebarMenu = function () {
        if($('ul.page-sidebar-menu li ul.sub-menu li').hasClass('active')){
            $('ul.page-sidebar-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').addClass('active');
            $('ul.page-sidebar-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').find('span.arrow').addClass('open');
        };
		
        if($('ul.page-sidebar-menu li ul.sub-menu li ul.sub-menu li').hasClass('active')){
            $('ul.page-sidebar-menu li ul.sub-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').parent('ul.sub-menu').parent('li').addClass('active');
            $('ul.page-sidebar-menu li ul.sub-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').parent('ul.sub-menu').parent('li').find('span.arrow').addClass('open');
        };

        $('ul.page-sidebar-menu li:nth-child(3)').addClass('start');
        $('ul.page-sidebar-menu li:nth-last-child(1)').addClass('last');
		
		jQuery('.page-sidebar').on('click', 'li > a', function (e) {
            if ($(this).next().hasClass('sub-menu') == false) {
                if ($('.btn-navbar').hasClass('collapsed') == false) {
                    $('.btn-navbar').click();
                }
                return;
            }

            if ($(this).next().hasClass('sub-menu.always-open')) {
                return;
            }

            var parent = $(this).parent().parent();
            var the = $(this);

            parent.children('li.open').children('a').children('.arrow').removeClass('open');
            parent.children('li.open').children('.sub-menu').slideUp(200);
            parent.children('li.open').removeClass('open');

            var sub = jQuery(this).next();
            var slideOffeset = -200;
            var slideSpeed = 200;

            if (sub.is(":visible")) {
                jQuery('.arrow', jQuery(this)).removeClass("open");
                jQuery(this).parent().removeClass("open");
                sub.slideUp(slideSpeed, function () {
                    if ($('body').hasClass('page-sidebar-fixed') == false && $('body').hasClass('page-sidebar-closed') == false) {
                        App.scrollTo(the, slideOffeset);
                    }
                    handleSidebarAndContentHeight();
                });
            } else {
                jQuery('.arrow', jQuery(this)).addClass("open");
                jQuery(this).parent().addClass("open");
                sub.slideDown(slideSpeed, function () {
                    if ($('body').hasClass('page-sidebar-fixed') == false && $('body').hasClass('page-sidebar-closed') == false) {
                        App.scrollTo(the, slideOffeset);
                    }
                    handleSidebarAndContentHeight();
                });
            }

            e.preventDefault();
        });

        if($('ul.page-sidebar-menu li ul.sub-menu li').hasClass('active')){
            $('ul.page-sidebar-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').addClass('active');
            $('ul.page-sidebar-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').find('span.arrow').addClass('open');
        };
        
        if($('ul.page-sidebar-menu li ul.sub-menu li ul.sub-menu li').hasClass('active')){
            $('ul.page-sidebar-menu li ul.sub-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').parent('ul.sub-menu').parent('li').addClass('active');
            $('ul.page-sidebar-menu li ul.sub-menu li ul.sub-menu').find('li.active').parent('ul.sub-menu').parent('li').parent('ul.sub-menu').parent('li').find('span.arrow').addClass('open');
        };

        $('ul.page-sidebar-menu li:nth-child(3)').addClass('start');
        $('ul.page-sidebar-menu li:nth-last-child(1)').addClass('last');

        // handle ajax links
        jQuery('.page-sidebar').on('click', ' li > a.ajaxify', function (e) {
            e.preventDefault();
            App.scrollTop();

            var url = $(this).attr("href");
            var menuContainer = jQuery('.page-sidebar ul');
            var pageContent = $('.page-content');
            var pageContentBody = $('.page-content .page-content-body');

            menuContainer.children('li.active').removeClass('active');
            menuContainer.children('arrow.open').removeClass('open');

            $(this).parents('li').each(function () {
                $(this).addClass('active');
                $(this).children('a > span.arrow').addClass('open');
            });
            $(this).parents('li').addClass('active');

            App.blockUI(pageContent, false);

            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function (res) {
                    App.unblockUI(pageContent);
                    pageContentBody.html(res);
                    App.fixContentHeight(); // fix content height
                    App.initAjax(); // initialize core stuff
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    pageContentBody.html('<h4>Could not load the requested content.</h4>');
                    App.unblockUI(pageContent);
                },
                async: false
            });
        });

        $('button.button-ajax').click(function(){
            var href = $(this).data('href');

            $.ajax({
                type: 'POST',
                cache: false,
                url: href,
                success: function(res){
                    $('#ajax-get-data').html(res);
                }   
            });
        });

        $('#saveajax').click(function(event){
            var button = $('#saveajax');
            var tablename = button.data('name');
            var value = [];

            //#transaksi .checkboxes
            $('form#'+tablename+' .checkboxes:checked').each(function(i){
                value[i] = $(this).val();
            });
            if ($("div#"+tablename+" ul.list-data li").length == 0) {
                for (count = 0; count <= value.length - 1; count++){
                    $('div#'+tablename+' ul.list-data').append("<li><span class='form-control-static'>"+value[count]+"</span></li>");
                    $('div#'+tablename+' .hidden-ajax-input').append("<input hidden type='text' name='check[]' value='"+value[count]+"'/>");
                }    
            }else{
                $("div#"+tablename+" ul.list-data li").remove();
                $("div#"+tablename+" .hidden-ajax-input :input").remove();
                for (count = 0; count <= value.length - 1; count++){
                    $('div#'+tablename+' ul.list-data').append("<li><span class='form-control-static'>"+value[count]+"</span></li>");
                    $('div#'+tablename+' .hidden-ajax-input').append("<input hidden type='text' name='check[]' value='"+value[count]+"'/>");
                }  
            }
            
            $('#ajax-get-data').modal('hide');
    }
    )};

    // Helper function to calculate sidebar height for fixed sidebar layout.
    var _calculateFixedSidebarViewportHeight = function () {
        var sidebarHeight = $(window).height() - $('.header').height() + 1;
        if ($('body').hasClass("page-footer-fixed")) {
            sidebarHeight = sidebarHeight - $('.footer').outerHeight();
        }

        return sidebarHeight;
    }

    // Handles fixed sidebar
    var handleFixedSidebar = function () {
        var menu = $('.page-sidebar-menu');

        if (menu.parent('.slimScrollDiv').size() === 1) { // destroy existing instance before updating the height
            menu.slimScroll({
                destroy: true
            });
            menu.removeAttr('style');
            $('.page-sidebar').removeAttr('style');
        }

        if ($('.page-sidebar-fixed').size() === 0) {
            handleSidebarAndContentHeight();
            return;
        }

        var viewport = _getViewPort();
        if (viewport.width >= 992) {
            var sidebarHeight = _calculateFixedSidebarViewportHeight();

            menu.slimScroll({
                size: '7px',
                color: '#a1b2bd',
                opacity: .3,
                position: isRTL ? 'left' : 'right',
                height: sidebarHeight,
                allowPageScroll: false,
                disableFadeOut: false
            });
            handleSidebarAndContentHeight();
        }
    }

    // Handles the sidebar menu hover effect for fixed sidebar.
    var handleFixedSidebarHoverable = function () {
        if ($('body').hasClass('page-sidebar-fixed') === false) {
            return;
        }

        $('.page-sidebar').off('mouseenter').on('mouseenter', function () {
            var body = $('body');

            if ((body.hasClass('page-sidebar-closed') === false || body.hasClass('page-sidebar-fixed') === false) || $(this).hasClass('page-sidebar-hovering')) {
                return;
            }

            body.removeClass('page-sidebar-closed').addClass('page-sidebar-hover-on');
            $(this).addClass('page-sidebar-hovering');
            $(this).animate({
                width: sidebarWidth
            }, 400, '', function () {
                $(this).removeClass('page-sidebar-hovering');
            });
        });

        $('.page-sidebar').off('mouseleave').on('mouseleave', function () {
            var body = $('body');

            if ((body.hasClass('page-sidebar-hover-on') === false || body.hasClass('page-sidebar-fixed') === false) || $(this).hasClass('page-sidebar-hovering')) {
                return;
            }

            $(this).addClass('page-sidebar-hovering');
            $(this).animate({
                width: sidebarCollapsedWidth
            }, 400, '', function () {
                $('body').addClass('page-sidebar-closed').removeClass('page-sidebar-hover-on');
                $(this).removeClass('page-sidebar-hovering');
            });
        });
    }
	
	var handleMenuCheck = function () {
        $('.parent-menu .group-check').each(function (){
            var checked = $(this).is(':checked');
            var set = '.child-menu .group-check-sec';
            $(this).click(function (){
                if($(this).is(':checked')){
                    $(this).parents('ul').find('ul.child-menu').children().find('.group-check-sec, .check-menu').attr('checked', true);
                }else{
                    $(this).parents('ul').find('ul.child-menu').children().find('.group-check-sec, .check-menu').attr('checked', false);
                }
                jQuery.uniform.update('.child-menu .group-check-sec, .grand-menu .check-menu');
            });            
        });
        $('.child-menu .group-check-sec').each(function (){
            var checked = $(this).is(':checked');
            var set = '.grand-menu .check-menu';
            $(this).click(function (){
                if($(this).is(':checked')){
                    $(this).parent().parent().parent().parent().find('ul.grand-menu').children().find('.check-menu').attr('checked', true);
                    $(this).parents('ul.parent-menu').find('.group-check').attr('checked', true);
                }else{
                    console.log();
                    if ($(this).closest('ul').find('.group-check-sec:checked').length === 0){
                        $(this).parents('ul.parent-menu').find('.group-check').attr('checked', false);
                    }else{
                        $(this).parents('ul.parent-menu').find('.group-check').attr('checked', true);
                    }
                    $(this).parent().parent().parent().parent().find('ul.grand-menu').children().find('.check-menu').attr('checked', false);
                }
                jQuery.uniform.update('.check-menu, .group-check');
            });            
        });
        $('.check-menu').each(function (){
            $(this).click(function(){
                if($(this).is(':checked')){
                    $(this).closest('ul').closest('li').find('.group-check-sec').attr('checked', true);
                    $(this).parents('ul.parent-menu').find('.group-check').attr('checked', true);
                }else{
                    if($(this).closest('ul').find('.check-menu:checked').length === 0){
                      $(this).closest('ul').closest('li').find('.group-check-sec').attr('checked', false);  
                    }else{
                        $(this).closest('ul').closest('li').find('.group-check-sec').attr('checked', true);  
                    }
                    if($(this).parents('ul.child-menu').find('.group-check-sec:checked').length === 0){
                        $(this).parents('ul.parent-menu').find('.group-check').attr('checked', false);  
                    }else{
                        $(this).parents('ul.parent-menu').find('.group-check').attr('checked', true);
                    }
                }
                jQuery.uniform.update('.group-check-sec, .group-check');
            });
        });
    }

    // Handles sidebar toggler to close/hide the sidebar.
    var handleSidebarToggler = function () {
        var viewport = _getViewPort();
        if ($.cookie('sidebar_closed') === '1' && viewport.width >= 992) {
            $('body').addClass('page-sidebar-closed');
        }

        // handle sidebar show/hide
        $('.page-sidebar').on('click', '.sidebar-toggler', function (e) {
            var body = $('body');
            var sidebar = $('.page-sidebar');

            if ((body.hasClass("page-sidebar-hover-on") && body.hasClass('page-sidebar-fixed')) || sidebar.hasClass('page-sidebar-hovering')) {
                body.removeClass('page-sidebar-hover-on');
                sidebar.css('width', '').hide().show();
                $.cookie('sidebar_closed', '0');
                e.stopPropagation();
                runResponsiveHandlers();
                return;
            }

            $(".sidebar-search", sidebar).removeClass("open");

            if (body.hasClass("page-sidebar-closed")) {
                body.removeClass("page-sidebar-closed");
                if (body.hasClass('page-sidebar-fixed')) {
                    sidebar.css('width', '');
                }
                $.cookie('sidebar_closed', '0');
            } else {
                body.addClass("page-sidebar-closed");
                $.cookie('sidebar_closed', '1');
            }
            runResponsiveHandlers();
        });

        // handle the search bar close
        $('.page-sidebar').on('click', '.sidebar-search .remove', function (e) {
            e.preventDefault();
            $('.sidebar-search').removeClass("open");
        });

        // handle the search query submit on enter press
        $('.page-sidebar').on('keypress', '.sidebar-search input', function (e) {
            if (e.which == 13) {
                $('.sidebar-search').submit();
                return false; //<---- Add this line
            }
        });

        // handle the search submit
        $('.sidebar-search .submit').on('click', function (e) {
            e.preventDefault();
            if ($('body').hasClass("page-sidebar-closed")) {
                if ($('.sidebar-search').hasClass('open') == false) {
                    if ($('.page-sidebar-fixed').size() === 1) {
                        $('.page-sidebar .sidebar-toggler').click(); //trigger sidebar toggle button
                    }
                    $('.sidebar-search').addClass("open");
                } else {
                    $('.sidebar-search').submit();
                }
            } else {
                $('.sidebar-search').submit();
            }
        });

        $('.sidebar-search a.menu_submit').click(function(){
            var menu = $('.menu_search').val().toLowerCase();         
            if (menu.length === 0) {
                return false;
            }else{
                if($('ul.page-sidebar-menu li:contains('+menu+')').length != 0){
                    // window.alert($('ul.page-sidebar-menu li:contains('+menu+')').length);
                    $('ul.page-sidebar-menu li:contains('+menu+')').addClass('active');  
                    $('input.menu_search').change(function(){
                        $('ul.page-sidebar-menu li:contains('+menu+')').removeClass('active');    
                    });
                }else{
                    $('ul.page-sidebar-menu li:contains('+menu+')').removeClass('active');
                }
            };
            $('input.menu_search').change(function(){
                $('ul.page-sidebar-menu li:contains('+menu+')').removeClass('active');    
            });
            
        });
    }

    // Handles the horizontal menu
    var handleHorizontalMenu = function () {
        //handle hor menu search form toggler click
        $('.header').on('click', '.hor-menu .hor-menu-search-form-toggler', function (e) {
            if ($(this).hasClass('off')) {
                $(this).removeClass('off');
                $('.header .hor-menu .search-form').hide();
            } else {
                $(this).addClass('off');
                $('.header .hor-menu .search-form').show();
            }
            e.preventDefault();
        });

        //handle hor menu search button click
        $('.header').on('click', '.hor-menu .search-form .btn', function (e) {
            $('.form-search').submit();
            e.preventDefault();
        });

        //handle hor menu search form on enter press
        $('.header').on('keypress', '.hor-menu .search-form input', function (e) {
            if (e.which == 13) {
                $('.form-search').submit();
                return false;
            }
        });
    }

    // Handles the go to top button at the footer
    var handleGoTop = function () {
        /* set variables locally for increased performance */
        jQuery('.footer').on('click', '.go-top', function (e) {
            App.scrollTo();
            e.preventDefault();
        });
    }

    // Handles portlet tools & actions
    var handlePortletTools = function () {
        jQuery('body').on('click', '.portlet > .portlet-title > .tools > a.remove', function (e) {
            e.preventDefault();
            jQuery(this).closest(".portlet").remove();
        });

        jQuery('body').on('click', '.portlet > .portlet-title > .tools > a.reload', function (e) {
            e.preventDefault();
            var el = jQuery(this).closest(".portlet").children(".portlet-body");
            App.blockUI(el);
            window.setTimeout(function () {
                App.unblockUI(el);
            }, 1000);
        });

        jQuery('body').on('click', '.portlet > .portlet-title > .tools > .collapse, .portlet .portlet-title > .tools > .expand', function (e) {
            e.preventDefault();
            var el = jQuery(this).closest(".portlet").children(".portlet-body");
            if (jQuery(this).hasClass("collapse")) {
                jQuery(this).removeClass("collapse").addClass("expand");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("expand").addClass("collapse");
                el.slideDown(200);
            }
        });
    }

    // Handles custom checkboxes & radios using jQuery Uniform plugin
    var handleUniform = function () {
        if (!jQuery().uniform) {
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)");
        if (test.size() > 0) {
            test.each(function () {
                if ($(this).parents(".checker").size() == 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }
    }

    // Handles Bootstrap Accordions.
    var handleAccordions = function () {
        var lastClicked;
        //add scrollable class name if you need scrollable panes
        jQuery('body').on('click', '.accordion.scrollable .accordion-toggle', function () {
            lastClicked = jQuery(this);
        }); //move to faq section

        jQuery('body').on('show.bs.collapse', '.accordion.scrollable', function () {
            jQuery('html,body').animate({
                scrollTop: lastClicked.offset().top - 150
            }, 'slow');
        });
    }

    // Handles Bootstrap Tabs.
    var handleTabs = function () {
        // fix content height on tab click
        $('body').on('shown.bs.tab', '.nav.nav-tabs', function () {
            handleSidebarAndContentHeight();
        });

        //activate tab if tab id provided in the URL
        if (location.hash) {
            var tabid = location.hash.substr(1);
            $('a[href="#' + tabid + '"]').click();
        }
    }

    // Handles Bootstrap Modals.
    var handleModals = function () {

        // fix stackable modal issue: when 2 or more modals opened, closing one of modal will remove .modal-open class. 
        $('body').on('hide.bs.modal', function () {
           if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') == false) {
              $('html').addClass('modal-open');
           } else if ($('.modal:visible').size() <= 1) {
              $('html').removeClass('modal-open');
           }
        });
    }

    // Handles Bootstrap Tooltips.
    var handleTooltips = function () {
       jQuery('.tooltips').tooltip();
    }

    // Handles Bootstrap Dropdowns
    var handleDropdowns = function () {
        /*
          For touch supported devices disable the 
          hoverable dropdowns - data-hover="dropdown"  
        */
        if (App.isTouchDevice()) {
            $('[data-hover="dropdown"]').each(function(){
                $(this).parent().off("hover");
                $(this).off("hover");
            });
        }
        /*
          Hold dropdown on click  
        */
        $('body').on('click', '.dropdown-menu.hold-on-click', function (e) {
            e.stopPropagation();
        })
    }

    // Handle Hower Dropdowns
    var handleDropdownHover = function () {
        $('[data-hover="dropdown"]').dropdownHover();
    }

    // Handles Bootstrap Popovers

    // last popep popover
    var lastPopedPopover;

    var handlePopovers = function () {
        jQuery('.popovers').popover();

        // close last poped popover

        $(document).on('click.bs.popover.data-api', function (e) {
            if (lastPopedPopover) {
                lastPopedPopover.popover('hide');
            }
        });
    }

    // Handles scrollable contents using jQuery SlimScroll plugin.
    var handleScrollers = function () {
        $('.scroller').each(function () {
            var height;
            if ($(this).attr("data-height")) {
                height = $(this).attr("data-height");
            } else {
                height = $(this).css('height');
            }
            $(this).slimScroll({
                size: '7px',
                color: ($(this).attr("data-handle-color")  ? $(this).attr("data-handle-color") : '#a1b2bd'),
                railColor: ($(this).attr("data-rail-color")  ? $(this).attr("data-rail-color") : '#333'),
                position: isRTL ? 'left' : 'right',
                height: height,
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });
        });
    }

    // Handles Image Preview using jQuery Fancybox plugin
    var handleFancybox = function () {
        if (!jQuery.fancybox) {
            return;
        }

        if (jQuery(".fancybox-button").size() > 0) {
            jQuery(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
    }

    // Fix input placeholder issue for IE8 and IE9
    var handleFixInputPlaceholderForIE = function () {
        //fix html5 placeholder attribute for ie7 & ie8
        if (isIE8 || isIE9) { // ie8 & ie9
            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            jQuery('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {

                var input = jQuery(this);

                if (input.val() == '' && input.attr("placeholder") != '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    // Handle full screen mode toggle
    var handleFullScreenMode = function() {
        // mozfullscreenerror event handler
       
        // toggle full screen
        function toggleFullScreen() {
          if (!document.fullscreenElement &&    // alternative standard method
              !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
            if (document.documentElement.requestFullscreen) {
              document.documentElement.requestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
              document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
              document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
          } else {
            if (document.cancelFullScreen) {
              document.cancelFullScreen();
            } else if (document.mozCancelFullScreen) {
              document.mozCancelFullScreen();
            } else if (document.webkitCancelFullScreen) {
              document.webkitCancelFullScreen();
            }
          }
        }

        $('#trigger_fullscreen').click(function() {
            toggleFullScreen();
        });
    }

    // Handle Select2 Dropdowns
    var handleSelect2 = function() {
        if (jQuery().select2) {
            $('select.select2me').select2({
                placeholder: "Select",
                allowClear: true
            });
        }
    }

    // Handle Theme Settings
    var handleTheme = function () {

        var panel = $('.theme-panel');

        if ($('body').hasClass('page-boxed') == false) {
            $('.layout-option', panel).val("fluid");
        }

        $('.sidebar-option', panel).val("default");
        $('.header-option', panel).val("fixed");
        $('.footer-option', panel).val("default");

        //handle theme layout
        var resetLayout = function () {
            $("body").
            removeClass("page-boxed").
            removeClass("page-footer-fixed").
            removeClass("page-sidebar-fixed").
            removeClass("page-header-fixed");

            $('.header > .header-inner').removeClass("container");

            if ($('.page-container').parent(".container").size() === 1) {
                $('.page-container').insertAfter('body > .clearfix');
            }

            if ($('.footer > .container').size() === 1) {
                $('.footer').html($('.footer > .container').html());
            } else if ($('.footer').parent(".container").size() === 1) {
                $('.footer').insertAfter('.page-container');
            }

            $('body > .container').remove();
        }

        var lastSelectedLayout = '';

        var setLayout = function () {

            var layoutOption = $('.layout-option', panel).val();
            var sidebarOption = $('.sidebar-option', panel).val();
            var headerOption = $('.header-option', panel).val();
            var footerOption = $('.footer-option', panel).val();

            if (sidebarOption == "fixed" && headerOption == "default") {
                alert('Default Header with Fixed Sidebar option is not supported. Proceed with Fixed Header with Fixed Sidebar.');
                $('.header-option', panel).val("fixed");
                $('.sidebar-option', panel).val("fixed");
                sidebarOption = 'fixed';
                headerOption = 'fixed';
            }

            resetLayout(); // reset layout to default state

            if (layoutOption === "boxed") {
                $("body").addClass("page-boxed");

                // set header
                $('.header > .header-inner').addClass("container");
                var cont = $('body > .clearfix').after('<div class="container"></div>');

                // set content
                $('.page-container').appendTo('body > .container');

                // set footer
                if (footerOption === 'fixed') {
                    $('.footer').html('<div class="container">' + $('.footer').html() + '</div>');
                } else {
                    $('.footer').appendTo('body > .container');
                }
            }

            if (lastSelectedLayout != layoutOption) {
                //layout changed, run responsive handler:
                runResponsiveHandlers();
            }
            lastSelectedLayout = layoutOption;

            //header
            if (headerOption === 'fixed') {
                $("body").addClass("page-header-fixed");
                $(".header").removeClass("navbar-static-top").addClass("navbar-fixed-top");
            } else {
                $("body").removeClass("page-header-fixed");
                $(".header").removeClass("navbar-fixed-top").addClass("navbar-static-top");
            }

            //sidebar
            if (sidebarOption === 'fixed') {
                $("body").addClass("page-sidebar-fixed");
            } else {
                $("body").removeClass("page-sidebar-fixed");
            }

            //footer 
            if (footerOption === 'fixed') {
                $("body").addClass("page-footer-fixed");
            } else {
                $("body").removeClass("page-footer-fixed");
            }

            handleSidebarAndContentHeight(); // fix content height            
            handleFixedSidebar(); // reinitialize fixed sidebar
            handleFixedSidebarHoverable(); // reinitialize fixed sidebar hover effect
        }

        // handle theme colors
        var setColor = function (color) {
            $('#style_color').attr("href", "assets/css/themes/" + color + ".css");
            $.cookie('style_color', color);
        }        


        $('.toggler', panel).click(function () {
            $('.toggler').hide();
            $('.toggler-close').show();
            $('.theme-panel > .theme-options').show();
        });

        $('.toggler-close', panel).click(function () {
            $('.toggler').show();
            $('.toggler-close').hide();
            $('.theme-panel > .theme-options').hide();
        });

        $('.theme-colors > ul > li', panel).click(function () {
            var color = $(this).attr("data-style");
            setColor(color);
            $('ul > li', panel).removeClass("current");
            $(this).addClass("current");
        });

        $('.layout-option, .header-option, .sidebar-option, .footer-option', panel).change(setLayout);

        if ($.cookie('style_color')) {
            setColor($.cookie('style_color'));
        }
    }


    //* END:CORE HANDLERS *//

    return {

        //main function to initiate the theme
        init: function () {

            //IMPORTANT!!!: Do not modify the core handlers call order.

            //core handlers
            handleInit(); // initialize core variables
            handleResponsiveOnResize(); // set and handle responsive    
            handleUniform(); // hanfle custom radio & checkboxes
            handleScrollers(); // handles slim scrolling contents 
            handleResponsiveOnInit(); // handler responsive elements on page load

            //layout handlers
            handleFixedSidebar(); // handles fixed sidebar menu
            handleFixedSidebarHoverable(); // handles fixed sidebar on hover effect 
            handleSidebarMenu(); // handles main menu
            handleHorizontalMenu(); // handles horizontal menu
            handleSidebarToggler(); // handles sidebar hide/show            
            handleFixInputPlaceholderForIE(); // fixes/enables html5 placeholder attribute for IE9, IE8
            handleGoTop(); //handles scroll to top functionality in the footer
            handleTheme(); // handles style customer tool

            //ui component handlers
            handleFancybox() // handle fancy box
            handleSelect2(); // handle custom Select2 dropdowns
            handlePortletTools(); // handles portlet action bar functionality(refresh, configure, toggle, remove)
            handleDropdowns(); // handle dropdowns
            handleTabs(); // handle tabs
            handleTooltips(); // handle bootstrap tooltips
            handlePopovers(); // handles bootstrap popovers
            handleAccordions(); //handles accordions 
            handleModals(); // handle modals
            handleFullScreenMode(); // handles full screen
			handleMenuCheck();
			handleAddButton();
			
			if(typeof google === 'object' && typeof google.maps === 'object'){
                    handleMaps();
            }
        },

        //main function to initiate core javascript after ajax complete
        initAjax: function () {
            handleSelect2(); // handle custom Select2 dropdowns
            handleDropdowns(); // handle dropdowns
            handleTooltips(); // handle bootstrap tooltips
            handlePopovers(); // handles bootstrap popovers
            handleAccordions(); //handles accordions 
            handleUniform(); // hanfle custom radio & checkboxes     
            handleDropdownHover() // handles dropdown hover   
			
			
        },

        //public function to fix the sidebar and content height accordingly
        fixContentHeight: function () {
            handleSidebarAndContentHeight();
        },

        //public function to remember last opened popover that needs to be closed on click
        setLastPopedPopover: function (el) {
            lastPopedPopover = el;
        },

        //public function to add callback a function which will be called on window resize
        addResponsiveHandler: function (func) {
            responsiveHandlers.push(func);
        },

        // useful function to make equal height for contacts stand side by side
        setEqualHeight: function (els) {
            var tallestEl = 0;
            els = jQuery(els);
            els.each(function () {
                var currentHeight = $(this).height();
                if (currentHeight > tallestEl) {
                    tallestColumn = currentHeight;
                }
            });
            els.height(tallestEl);
        },

        // wrapper function to scroll(focus) to an element
        scrollTo: function (el, offeset) {
            pos = (el && el.size() > 0) ? el.offset().top : 0;
            jQuery('html,body').animate({
                scrollTop: pos + (offeset ? offeset : 0)
            }, 'slow');
        },

        // function to scroll to the top
        scrollTop: function () {
            App.scrollTo();
        },

        // wrapper function to  block element(indicate loading)
        blockUI: function (el, centerY) {
            var el = jQuery(el);
            if (el.height() <= 400) {
                centerY = true;
            }
            el.block({
                message: '<img src="../../assets/img/ajax-loading.gif" align="">',
                centerY: centerY != undefined ? centerY : true,
                css: {
                    top: '10%',
                    border: 'none',
                    padding: '2px',
                    backgroundColor: 'none'
                },
                overlayCSS: {
                    backgroundColor: '#000',
                    opacity: 0.05,
                    cursor: 'wait'
                }
            });
        },

        // wrapper function to  un-block element(finish loading)
        unblockUI: function (el) {
            jQuery(el).unblock({
                onUnblock: function () {
                    jQuery(el).removeAttr("style");
                }
            });
        },

        // initializes uniform elements
        initUniform: function (els) {
            if (els) {
                jQuery(els).each(function () {
                    if ($(this).parents(".checker").size() == 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
            } else {
                handleUniform();
            }

        },

        //wrapper function to update/sync jquery uniform checkbox & radios
        updateUniform: function (els) {
            $.uniform.update(els); // update the uniform checkbox & radios UI after the actual input control state changed
        },

        //public function to initialize the fancybox plugin
        initFancybox: function () {
            handleFancybox();
        },

        //public helper function to get actual input value(used in IE9 and IE8 due to placeholder attribute not supported)
        getActualVal: function (el) {
            var el = jQuery(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }
            return el.val();
        },

        //public function to get a paremeter by name from URL
        getURLParameter: function (paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        // check for device touch support
        isTouchDevice: function () {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        // check IE8 mode
        isIE8: function () {
            return isIE8;
        },

        // check IE9 mode
        isIE9: function () {
            return isIE9;
        },

        //check RTL mode
        isRTL: function () {
            return isRTL;
        },

        // get layout color code by color name
        getLayoutColorCode: function (name) {
            if (layoutColorCodes[name]) {
                return layoutColorCodes[name];
            } else {
                return '';
            }
        }

    };

}();