<?php

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'dashboard/');
        $this->data = array();
        init_generic_dao();
        $this->load->library(array('template_admin'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
    }

    public function index() {
        $this->template_admin->display('welcome_message');
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin");
        }
    }

}

?>