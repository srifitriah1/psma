<?php

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        init_generic_dao();
        $this->load->helper(array('form', 'url', 'html'));
        $this->load->model('m_users', '', TRUE);
        $this->load->library('session');
    }

    function index() {
        if (($this->session->userdata('logged_in') == TRUE)) {
            redirect('admin/dashboard');
        } else {
            $data['message'] = "";
            $this->load->view('admin/login', $data);
        }
    }

    function auth() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->m_users->check_login($username, $password);
        if ($result == TRUE) {
            $newdata = array(
                'username' => $username,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            redirect('admin/dashboard');
        } else {
            $data['message'] = "Login Unsuccessfull, Please try Again!";
            $this->load->view('admin/login', $data);
        }
    }

     public function forgot_password(){
        //Email configuration
        $config_email = "arisman87@gmail.com";
        $config_password = "arisman28";
        $email = $this->input->post('email');
        // $this->load->library('rest', array('server' => server_url(),
        //     'http_user' => null,
        //     'http_pass' => null,
        //     'http_auth' => 'digest'));
        // $this->rest->format("application/json");
        $data = array('user_email'=> $email);
        $new_password = random_string('alnum', 7);
        $edit_user = array('user_password' => md5($new_password));
        $result = $this->m_users->UpdateData('users',$edit_user,$data);
        
        //print_r($result);die();
        // $result = $this->rest->post('login/forgot_password', $data);
        if($result == 1) {
            // $data = $result->result;
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => $config_email,
                'smtp_pass' => $config_password,
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

            $this->email->from($config_email, 'psma');
            $this->email->to($email); 

            $this->email->subject('Kata Kunci Baru untuk Akun PSMA');
            $this->email->message('Kata kunci Anda yang baru adalah : '.$new_password);  

            $result = $this->email->send();
            $this->session->set_flashdata(array('message' => 'Kata kunci baru sudah dikirim ke email Anda.', 'type_message' => 'success'));
        } else {
            $this->session->set_flashdata(array('message' => 'Maaf, email Anda tidak terdaftar.', 'type_message' => 'error'));
        }
        redirect(base_url() . 'admin/login');
    }

    function logout() {
        $newdata = array(
            'username' => '',
            'logged_in' => FALSE
        );
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect('admin/login');
    }

}

?>