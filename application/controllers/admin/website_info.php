<?php

class Website_info extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/website_info/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_website_info'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
    }

    private function validate() {
        $this->form_validation->set_rules('id', 'Id', 'trim|required|max_length[10]|integer');
        $this->form_validation->set_rules('title', 'Title', 'trim|max_length[255]');
        $this->form_validation->set_rules('meta_title', 'Meta Title', 'trim|max_length[255]');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'trim');
        $this->form_validation->set_rules('address_title', 'Address Title', 'trim|max_length[255]');
        $this->form_validation->set_rules('address', 'Address', 'trim|max_length[255]');
        // $this->form_validation->set_rules('address_latitude', 'Address Latitude', 'trim|max_length[12]|numeric');
        // $this->form_validation->set_rules('address_longitude', 'Address Longitude', 'trim|max_length[12]|numeric');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|max_length[50]');
        $this->form_validation->set_rules('fax', 'Fax', 'trim|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'trim|max_length[100]|valid_email');
        $this->form_validation->set_rules('live_tv', 'Live Tv', 'trim|max_length[255]');
        $this->form_validation->set_rules('twitter', 'Twitter', 'trim|max_length[100]');
        $this->form_validation->set_rules('facebook', 'Facebook', 'trim|max_length[100]');
        $this->form_validation->set_rules('youtube', 'Youtube', 'trim|max_length[255]');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->detail(1);
    }

    public function fetch_record($keys) {
        $this->data['website_info'] = $this->m_website_info->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['website_info'] = $this->m_website_info->fetch($limit, $offset, null, true, $key);
    }

    private function fetch_input() {
        $data = array('id' => $this->input->post('id'),
            'title' => $this->input->post('title'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'address_title' => $this->input->post('address_title'),
            'address' => $this->input->post('address'),
            'address_latitude' => $this->input->post('address_latitude'),
            'address_longitude' => $this->input->post('address_longitude'),
            'phone' => $this->input->post('phone'),
            'fax' => $this->input->post('fax'),
            'email' => $this->input->post('email'),
            'live_tv' => $this->input->post('live_tv'),
            'twitter' => $this->input->post('twitter'),
            'facebook' => $this->input->post('facebook'),
            'youtube' => $this->input->post('youtube'));

        return $data;
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($id) {
        $obj = $this->fetch_input();
		//print_r($obj);die();
        $obj_id = array('id' => $id);

        if ($this->validate() != false) {
            $this->m_website_info->update($obj, $obj_id);
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            #prepare link for back to view list
            $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'detail' . '/' . $id, 'Back', array('class' => 'back'));
            $this->template_admin->display('website_info/website_info_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($id) {
        $obj_id = array('id' => $id);

        $this->preload();
        $this->fetch_record($obj_id);
        #prepare link for back to view list
        $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'index/', 'Back', array('class' => 'back'));
        $this->template_admin->display('website_info/website_info_detail', $this->data);
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin");
        }
    }

}

?>