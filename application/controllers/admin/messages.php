<?php

class Messages extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/messages/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_messages'));
        $this->load->library(array('template_admin','public_function'));
        $this->logged_in();
    }

    private function validate() {
        $this->form_validation->set_rules('message_name', 'Message Name', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('message_email', 'Message Email', 'trim|required|max_length[100]|valid_email');
        $this->form_validation->set_rules('message_title', 'Message Title', 'trim|max_length[255]');
        $this->form_validation->set_rules('message_content', 'Message Content', 'trim|required');
        $this->form_validation->set_rules('message_date', 'Message Date', 'trim|required');
        $this->form_validation->set_rules('message_status', 'Message Status', 'trim|required|max_length[3]|integer');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array(
            'filter_messages' => array(
                'message_name' => '',
                'message_email' => '',
                
                ))
        );
        $offset = ($page - 1) * $this->limit;
		$this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['messages'] = $this->m_messages->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['messages'] = $this->m_messages->fetch($limit, $offset, $order_by='message_status', true, $key);
    }

    private function fetch_input() {
        $data = array('message_name' => $this->input->post('message_name'),
            'message_email' => $this->input->post('message_email'),
            'message_title' => $this->input->post('message_title'),
            'message_content' => $this->input->post('message_content'),
            'message_date' => $this->input->post('message_date'),
            'message_status' => $this->input->post('message_status'));

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();

        if ($this->validate() != false) {
            $this->m_messages->insert($obj);
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['messages'] = (object) $obj;
            #prepare link for back to view list
            $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'index/', 'Back', array('class' => 'back'));
            $this->template_admin->display('messages/messages_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($message_id) {
		$obj['message_status'] = "1";
        $obj_id = array('message_id' => $message_id);
            $this->m_messages->update($obj, $obj_id);
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($message_id) {
        $obj_id = array('message_id' => $message_id);

        $this->preload();
		$this->edit($message_id);
        $this->fetch_record($obj_id);
        #prepare link for back to view list
        $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'index/', 'Back', array('class' => 'back'));
        $this->template_admin->display('messages/messages_detail', $this->data);
    }

    public function delete($message_id) {
        $obj_id = array('message_id' => $message_id);

        $this->m_messages->delete($obj_id);
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        redirect(CURRENT_CONTEXT);
    }

    public function delete_multiple() {
        $data = file_get_contents('php://input');
        $id = json_decode($data);
        foreach ($id->ids as $id) {
            $obj_id = array('message_id' => $id->message_id);
            $this->m_messages->delete($obj_id);
        }
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        echo json_encode(array('status' => 200));
    }

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_messages');
        if ($this->input->post('search')) {
            $key = array(
                'message_name' => $this->input->post('message_name'),
                'message_email' => $this->input->post('message_email')
            );
            $this->session->set_userdata(array('filter_messages' => $key));
        }
        $offset = ($page - 1) * $this->limit;
		$this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset, $key);
    }

    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->data['total_rows'] = $this->m_messages->count_all($key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key)) ? 'search' : 'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->fetch_data($limit, $offset, $key);
        $this->template_admin->display('messages/messages_list', $this->data);
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin");
        }
    }

}

?>