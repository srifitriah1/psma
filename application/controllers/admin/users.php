<?php

class Users extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    // construct menyimpan data yang di perlukan
    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/users/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_users', 'm_role'));
        $this->load->library(array('template_admin','public_function'));
        $this->logged_in();
		$this->path = "upload/pengguna/";
    }

    //validate untuk validasi
    //'nama dari inputan','message',''
    private function validate($edit) {
        $this->form_validation->set_rules('role_id', 'Role Id', 'trim|required|max_length[10]|integer');
        $this->form_validation->set_rules('user_fullname', 'User Fullname', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('user_username', 'User Username', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('user_email', 'User Email', 'trim|required|max_length[100]|valid_email');
        if($edit){
			$this->form_validation->set_rules('user_password', 'User Password', 'trim|required|max_length[255]');
		}
        $this->form_validation->set_rules('user_photo', 'User Photo', 'callback_handle_upload');
        $this->form_validation->set_rules('user_registered_on', 'User Registered On', 'trim');
        $this->form_validation->set_rules('user_status', 'User Status', 'trim|required|max_length[3]|integer');

        return $this->form_validation->run();
        // untuk memberikan / pengecekan validasi
    }
	
	function handle_upload() {
        $param = $_FILES['user_photo'];
        if (!empty($param['name'])) {
            $_FILES['userfile'] = $param;
            $config = array();
            $config['upload_path'] = "./" . $this->path;
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = '100000';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $this->file_link = $this->path . $upload_data['file_name'];
                return true;
            } else {
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            $this->file_link = null;
            return true;
        }
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['role_list'] = $this->m_role->fetch();
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array(
            'filter_users' => array(
                'role_id' => '',
                'user_fullname' => ''))
        );
        $offset = ($page - 1) * $this->limit;
		$this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['users'] = $this->m_users->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['users'] = $this->m_users->fetch($limit, $offset, null, true, $key);
    }

    private function fetch_input() {
        $data = array('role_id' => $this->input->post('role_id'),
            'user_fullname' => $this->input->post('user_fullname'),
            'user_username' => $this->input->post('user_username'),
            'user_email' => $this->input->post('user_email'),
            'user_password' => md5($this->input->post('user_password')),
            'user_registered_on' => date('Y-m-d H-m-s'),
            'user_status' => $this->input->post('user_status'));

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();

        if ($this->validate(true) != false) {
			$obj['user_photo'] = $this->file_link;
            $this->m_users->insert($obj);
			$this->session->set_flashdata(array('message' => 'Data berhasil disimpan.', 'type_message' => 'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['users'] = (object) $obj;
            #prepare link for back to view list
            $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'index/', 'Back', array('class' => 'back'));
            $this->template_admin->display('users/users_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($user_id) {
        $obj = $this->fetch_input();

        $obj_id = array('user_id' => $user_id);

        if ($this->validate(false) != false) {
			if(!empty($this->file_link)){
                $obj['user_photo'] = $this->file_link;
                $data = $this->m_users->by_id($obj_id);
				$this->m_users->update($obj, $obj_id);
			}else{
				$this->m_users->update($obj, $obj_id);
			}
			$this->session->set_flashdata(array('message' => 'Data berhasil disimpan.', 'type_message' => 'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            #prepare link for back to view list
            $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'detail' . '/' . $user_id, 'Back', array('class' => 'back'));
            $this->template_admin->display('users/users_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($user_id) {
        $obj_id = array('user_id' => $user_id);

        $this->preload();
        $this->fetch_record($obj_id);
        #prepare link for back to view list
        $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'index/', 'Back', array('class' => 'back'));
        $this->template_admin->display('users/users_detail', $this->data);
    }

    public function delete($user_id) {
        $obj_id = array('user_id' => $user_id);

        $this->m_users->delete($obj_id);
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        redirect(CURRENT_CONTEXT);
    }

    public function delete_multiple() {
        $data = file_get_contents('php://input');
        $id = json_decode($data);
        foreach ($id->ids as $id) {
            $obj_id = array('user_id' => $id->user_id);
            $this->m_users->delete($obj_id);
        }
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        echo json_encode(array('status' => 200));
    }

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_users');
        if ($this->input->post('search')) {
            $key = array(
                'role_id' => $this->input->post('role_id'),
                'user_fullname' => $this->input->post('user_fullname')
            );
            $this->session->set_userdata(array('filter_users' => $key));
        }
        $offset = ($page - 1) * $this->limit;
		$this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset, $key);
    }


    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->data['total_rows'] = $this->m_users->count_all($key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key)) ? 'search' : 'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->fetch_data($limit, $offset, $key);
        // echo "<pre>";print_r($this->data); die();
        $this->template_admin->display('users/users_list', $this->data);
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin");
        }
    }

}

?>