<?php

class Menu_highlight extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/menu_highlight/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_menu_highlight'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
		$this->path = "upload/menu_highlight/";
    }

    private function validate() {
        $this->form_validation->set_rules('highlight_title', 'Highlight Title', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('highlight_image', 'Highlight Image', 'callback_handle_upload');
        $this->form_validation->set_rules('highlight_link', 'Highlight Link', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('highlight_status', 'Highlight Status', 'trim|required|max_length[3]|integer');

        return $this->form_validation->run();
    }
	
	function handle_upload() {
        $param = $_FILES['highlight_image'];
        if (!empty($param['name'])) {
            $_FILES['userfile'] = $param;
            $config = array();
            $config['upload_path'] = "./" . $this->path;
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = '100000';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $this->file_link = $this->path . $upload_data['file_name'];
                return true;
            } else {
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            $this->file_link = null;
            return true;
        }
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array(
            'filter_menu_highlight' => array(
                'highlight_title' => '',
                'highlight_image' => ''))
        );
        $offset = ($page - 1) * $this->limit;
		$this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['menu_highlight'] = $this->m_menu_highlight->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['menu_highlight'] = $this->m_menu_highlight->fetch($limit, $offset, null, true, $key);
    }

    private function fetch_input() {
        $data = array('highlight_title' => $this->input->post('highlight_title'),
            'highlight_image' => $this->input->post('highlight_image'),
            'highlight_link' => $this->input->post('highlight_link'),
            'highlight_status' => $this->input->post('highlight_status'));

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();

        if ($this->validate() != false) {
			$obj['highlight_image'] = $this->file_link;
            $this->m_menu_highlight->insert($obj);
			$this->session->set_flashdata(array('message' => 'Data berhasil disimpan.', 'type_message' => 'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['menu_highlight'] = (object) $obj;
            #prepare link for back to view list
            $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'index/', 'Back', array('class' => 'back'));
            $this->template_admin->display('menu_highlight/menu_highlight_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($highlight_id) {
        $obj = $this->fetch_input();

        $obj_id = array('highlight_id' => $highlight_id);

        if ($this->validate() != false) {
			if(!empty($this->file_link)){
				$obj['highlight_image'] = $this->file_link;
				$data = $this->m_menu_highlight->by_id($obj_id);
				$this->m_menu_highlight->update($obj, $obj_id);
				@unlink("./" . $data->highlight_image);
			}else{
				$this->m_menu_highlight->update($obj, $obj_id);
			}
			$this->session->set_flashdata(array('message' => 'Data berhasil disimpan.', 'type_message' => 'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            #prepare link for back to view list
            $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'detail' . '/' . $highlight_id, 'Back', array('class' => 'back'));
            $this->template_admin->display('menu_highlight/menu_highlight_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($highlight_id) {
        $obj_id = array('highlight_id' => $highlight_id);

        $this->preload();
        $this->fetch_record($obj_id);
        #prepare link for back to view list
        $this->data['link_back'] = anchor(CURRENT_CONTEXT . 'index/', 'Back', array('class' => 'back'));
        $this->template_admin->display('menu_highlight/menu_highlight_detail', $this->data);
    }

    public function delete($highlight_id) {
        $obj_id = array('highlight_id' => $highlight_id);

        $this->m_menu_highlight->delete($obj_id);
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        redirect(CURRENT_CONTEXT);
    }

    public function delete_multiple() {
        $data = file_get_contents('php://input');
        $id = json_decode($data);
        foreach ($id->ids as $id) {
            $obj_id = array('highlight_id' => $id->highlight_id);
            $this->m_menu_highlight->delete($obj_id);
        }
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        echo json_encode(array('status' => 200));
    }

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_menu_highlight');
        if ($this->input->post('search')) {
            $key = array(
                'highlight_title' => $this->input->post('highlight_title'),
                'highlight_image' => $this->input->post('highlight_image')
            );
            $this->session->set_userdata(array('filter_menu_highlight' => $key));
        }
        $offset = ($page - 1) * $this->limit;
		$this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset, $key);
    }

    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->data['total_rows'] = $this->m_menu_highlight->count_all($key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key)) ? 'search' : 'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->fetch_data($limit, $offset, $key);
        $this->template_admin->display('menu_highlight/menu_highlight_list', $this->data);
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin");
        }
    }

}

?>