<!-- BEGIN PAGE HEADER-->   
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">Link Internal</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="<?php echo base_url().'admin'; ?>">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>Link Internal</li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-search fa-fw"></i>&nbsp; Pencarian</div>
            </div>
            <div class="portlet-body">
                <form method="post" action="<?php echo $current_context . 'search'; ?>" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $key = (object) $this->session->userdata('filter_link_internal'); ?>
                            <div class="col-md-6"><div class="form-group">
                                    <label class="control-label col-md-5">Judul</label>
                                    <div class="col-md-7"><input class="form-control " name="link_title" value="<?php echo $key->link_title; ?>" placeholder="Judul Link">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6"><div class="form-group">
                                    <label class="control-label col-md-5">Link</label>
                                    <div class="col-md-7"><input class="form-control " name="link_link" value="<?php echo $key->link_link; ?>" placeholder="Link">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="search" name="search" value="true">
                            <div class="clearfix">
                                <button type="submit" class="btn green pull-right">Lakukan Pencarian</button>
                                <button type="button" class="btn default pull-right" onclick="location.href='<?php echo $current_context; ?>'">Kosongkan Pencarian</button>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php
    $message = $this->session->flashdata('message');
    $type_message = $this->session->flashdata('type_message');
    echo (!empty($message) && $type_message == "success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>' . $message . '</div></div>' : '';
    echo (!empty($message) && $type_message == "error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>' . $message . '</div></div>' : '';
    ?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-table fa-fw"></i>&nbsp; Tabel Data (<?php echo $total_rows; ?> Data)</div> <!-- SHOW TITLE AND NUMBER OF DATA -->
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="btn-group">
                        <div class="clearfix">
                            <a href="" id="deleteall" data-href="<?php echo $current_context . 'delete_multiple/'; ?>" class="btn red" disabled="true" data-toggle="modal" data-target="#del_All"><i class="fa fa-trash fa-fw"></i>&nbsp; Hapus Data</a>
                            <a id="" class="btn green" href="<?php echo $current_context . 'add/'; ?>">
                                Tambah &nbsp;<i class="fa fa-plus fa-fw"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" id="table_data">
                    <thead>
                        <tr>
                            <th class="table-checkbox"><input type="checkbox" class="group-checkable" data-set="#table_data .checkboxes" /></th>
                            <th>No</th>
							<th>Judul</th>
                            <th>Link</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						$i = 1;
                        foreach ($link_internal as $row) {
                            ?>
                            <tr class="odd gradeX">
                                <td><input type="checkbox" class="checkboxes"  data-link_id="<?php echo $row->link_id; ?>"  /></td>
                                <td><?php echo $i + $offset; ?></td>
								<td><?php echo $row->link_title; ?></td>
                                <td><?php echo $row->link_link; ?></td>
                                <td><?php echo ($row->link_status==1)?"Aktif":"Tidak Aktif"; ?></td>
                                <td>
                                    <a href="<?php echo $current_context . 'edit' . '/' . $row->link_id ?>" class="btn blue btn-xs"><i class="fa fa-edit fa-fw"></i>ubah</a>
                                    <a href="#" data-href="<?php echo $current_context . 'delete' . '/' . $row->link_id ?>" data-toggle="modal" data-target="#deleteModal"  class="btn red btn-xs"><i class="fa fa-trash fa-fw"></i>hapus</a>
                                </td>
                            </tr>
                        <?php 
						$i++;
						} ?>
                    </tbody>
                </table>
                <div>
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>