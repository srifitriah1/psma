<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">News Comment</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                News Comment
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Tambah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('parent_comment_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Parent Comment Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="parent_comment_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($news_comment_list as $news_comment){
						if($news_comment->comment_id == set_value('parent_comment_id',$news_comment->parent_comment_id)){
							echo "<option value='$news_comment->comment_id' selected>$news_comment->comment_id</option>";
						} else {
							echo "<option value='$news_comment->comment_id'>$news_comment->comment_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('parent_comment_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('news_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">News Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="news_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($news_list as $news){
						if($news->news_id == set_value('news_id',$news_comment->news_id)){
							echo "<option value='$news->news_id' selected>$news->news_id</option>";
						} else {
							echo "<option value='$news->news_id'>$news->news_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('news_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('comment_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Comment Name</label>
				<div class="col-md-9"><input class="form-control " name="comment_name" value="<?php echo set_value('comment_name', $news_comment->comment_name); ?>" placeholder="Comment Name"  required  maxlength=50>
				<?php echo form_error('comment_name'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('comment_email') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Comment Email</label>
				<div class="col-md-9"><input class="form-control " name="comment_email" value="<?php echo set_value('comment_email', $news_comment->comment_email); ?>" placeholder="Comment Email"  required  maxlength=100>
				<?php echo form_error('comment_email'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('comment_website') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Comment Website</label>
				<div class="col-md-9"><input class="form-control " name="comment_website" value="<?php echo set_value('comment_website', $news_comment->comment_website); ?>" placeholder="Comment Website"  maxlength=100>
				<?php echo form_error('comment_website'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('comment_content') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Comment Content</label>
				<div class="col-md-9"><textarea class="form-control" name="comment_content" ><?php echo set_value('comment_content', $news_comment->comment_content); ?></textarea>
				<?php echo form_error('comment_content'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('comment_avatar') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Comment Avatar</label>
				<div class="col-md-9"><input class="form-control " name="comment_avatar" value="<?php echo set_value('comment_avatar', $news_comment->comment_avatar); ?>" placeholder="Comment Avatar"  maxlength=255>
				<?php echo form_error('comment_avatar'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('comment_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Comment Status</label>
				<div class="col-md-9"><input class="form-control mask_number" name="comment_status" value="<?php echo set_value('comment_status', $news_comment->comment_status); ?>" placeholder="Comment Status"  required  maxlength=10>
				<?php echo form_error('comment_status'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>