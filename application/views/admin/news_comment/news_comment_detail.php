<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">News Comment</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                News Comment
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data News Comment</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Comment Id:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->comment_id; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Parent Comment Id:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->parent_comment_id; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">News Id:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->news_id; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Comment Name:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->comment_name; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Comment Email:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->comment_email; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Comment Website:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->comment_website; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Comment Content:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->comment_content; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Comment Avatar:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->comment_avatar; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Comment Status:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $news_comment->comment_status; ?></p>
					</div>
				</div>
			</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-6">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $news_comment->comment_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>