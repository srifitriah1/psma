<!-- BEGIN PAGE HEADER-->   
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Forum Discussion</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="<?php echo base_url(); ?>">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>Forum Discussion</li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->

<div class="row">
<?php
	$message = $this->session->flashdata('message');
	$type_message = $this->session->flashdata('type_message');
	echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
	echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-table fa-fw"></i>&nbsp; Tabel Data <!-- (<?php echo $total_rows;?> Data) --></div> <!-- SHOW TITLE AND NUMBER OF DATA -->
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="btn-group">
                        <div class="clearfix">
							<a href="" id="deleteall" data-href="<?php echo $current_context . 'delete_multiple/'; ?>" class="btn red" disabled="true" data-toggle="modal" data-target="#del_All"><i class="fa fa-trash fa-fw"></i>&nbsp; Hapus Data</a>
                            <a id="" class="btn green" href="<?php echo $current_context . 'add/'; ?>">
                                Tambah &nbsp;<i class="fa fa-plus fa-fw"></i>
                            </a>
                            <a class="btn purple" href="#" data-toggle="dropdown">
                                Kolom &nbsp;
                                <i class="fa fa-angle-down fa-fw"></i>
                            </a>
                            <div id="table_data_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                <label><input type="checkbox" checked data-column="0">Checkbox</label>
                                <label><input type="checkbox" checked data-column="1">No</label>
							
							<label><input type="checkbox" checked data-column="3">Fb Name</label>
							<label><input type="checkbox" checked data-column="4">Discuss Comment</label>
							<label><input type="checkbox" checked data-column="5">Discuss Datetime</label>
							<label><input type="checkbox" checked data-column="6">Discuss Status</label>
                            </div>

                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" id="table_data">
                    <thead>
                        <tr>
                            <th class="table-checkbox"><input type="checkbox" class="group-checkable" data-set="#table_data .checkboxes" /></th>
                            <th>No</th>
						<th>Fb Name</th>
						<th>Discuss Comment</th>
						<th>Discuss Datetime</th>
						<th>Discuss Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i=1;
                        foreach ($forum_discussion as $row) {
                            ?>
                            <tr class="odd gradeX">
                            <td><input type="checkbox" class="checkboxes"  data-discuss_id="<?php echo $row->discuss_id; ?>"/></td>
                            <td><?php echo $i + $offset; ?></td>
							
							<td><?php echo $row->fb_name; ?></td>
							<td><?php echo $row->discuss_comment; ?></td>
							<td><?php echo $row->discuss_datetime; ?></td>
                            <td><?php echo ($row->discuss_status==1)?"Tampil":"Tidak Tampil"; ?></td>
							<td>
								
								<a href="<?php echo $current_context . 'edit'  .'/'. $row->topic_id .'/'. $row->discuss_id .'/'. $row->discuss_status ?>" class="btn blue btn-xs"><i class="fa fa-edit fa-fw"></i><?php echo ($row->discuss_status ==1)? "Sembunyikan":"Tampilkan"; ?></a>
								</td>
                            </tr>
                        <?php 
                        $i++;
                        } ?>
                    </tbody>
                </table>
                <div>
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>