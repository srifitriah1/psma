<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Forum Discussion</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Forum Discussion
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Tambah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('topic_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Topic Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="topic_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($forum_topic_list as $forum_topic){
						if($forum_topic->topic_id == set_value('topic_id',$forum_discussion->topic_id)){
							echo "<option value='$forum_topic->topic_id' selected>$forum_topic->topic_id</option>";
						} else {
							echo "<option value='$forum_topic->topic_id'>$forum_topic->topic_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('topic_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('fb_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Fb Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="fb_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($fb_user_list as $fb_user){
						if($fb_user->fb_id == set_value('fb_id',$forum_discussion->fb_id)){
							echo "<option value='$fb_user->fb_id' selected>$fb_user->fb_id</option>";
						} else {
							echo "<option value='$fb_user->fb_id'>$fb_user->fb_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('fb_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('discuss_comment') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Discuss Comment</label>
				<div class="col-md-9"><textarea class="form-control" name="discuss_comment" ><?php echo set_value('discuss_comment', $forum_discussion->discuss_comment); ?></textarea>
				<?php echo form_error('discuss_comment'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('discuss_datetime') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Discuss Datetime</label>
				<div class="col-md-9"><div class="input-group date date-picker" data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
				<input type="text" name="discuss_datetime" class="form-control" readonly value="<?php echo set_value('discuss_datetime', $forum_discussion->discuss_datetime); ?>" placeholder="Discuss Datetime">
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
				<?php echo form_error('discuss_datetime'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('discuss_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Discuss Status</label>
				<div class="col-md-9"><input class="form-control mask_number" name="discuss_status" value="<?php echo set_value('discuss_status', $forum_discussion->discuss_status); ?>" placeholder="Discuss Status"  required  maxlength=3>
				<?php echo form_error('discuss_status'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>