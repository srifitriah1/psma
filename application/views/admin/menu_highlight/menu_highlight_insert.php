<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Menu Highlight</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Menu Highlight
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('highlight_title') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Judul</label>
				<div class="col-md-9"><input class="form-control " name="highlight_title" value="<?php echo set_value('highlight_title', $menu_highlight->highlight_title); ?>" placeholder="Judul"  required  maxlength=50>
				<?php echo form_error('highlight_title'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('highlight_image') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Foto</label>
				<div class="col-md-9">
				<?php
					if ($edit) {
						echo (empty($menu_highlight->highlight_image)) ? "" : "<img src='" . base_url() . $menu_highlight->highlight_image . "' width=200px>";
					}
					?>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="input-group">
							<span class="input-group-btn">
								<span class="uneditable-input">
									<i class="fa fa-file fileupload-exists"></i> 
									<span class="fileupload-preview"></span>
								</span>
							</span>
							<span class="btn default btn-file">
								<span class="fileupload-new"><i class="fa fa-paperclip"></i> Pilih File</span>
								<span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah File</span>
								<input type="file" class="default" name="highlight_image" placeholder="File" accept="image/*"/>
							</span>
							<a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Hapus</a>
						</div>
					</div>
					<?php echo ($edit && !empty($menu_highlight->highlight_image)) ? '<p class="help-block">Masukkan file baru jika ingin mengubah.</p>' : '' ?>
					<?php echo form_error('highlight_image'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('highlight_link') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Link</label>
				<div class="col-md-9"><input class="form-control " name="highlight_link" value="<?php echo set_value('highlight_link', $menu_highlight->highlight_link); ?>" placeholder="Link"  required  maxlength=255>
				<?php echo form_error('highlight_link'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('highlight_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="highlight_status" value="1" <?php echo set_value('highlight_status', ($menu_highlight->highlight_status == '1') ? "checked" : ""); ?>> Aktif
					</label>
					<label>
						<input type="radio" name="highlight_status" value="0" <?php echo set_value('highlight_status', ($menu_highlight->highlight_status === '0') ? "checked" : ""); ?>> Tidak Aktif
					</label>
				<?php echo form_error('highlight_status'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>