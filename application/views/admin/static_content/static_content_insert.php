<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Konten Statis</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Konten Statis
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('static_title') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Judul</label>
				<div class="col-md-9"><input class="form-control " name="static_title" value="<?php echo set_value('static_title', $static_content->static_title); ?>" placeholder="Judul"  required  maxlength=100>
				<?php echo form_error('static_title'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('static_content') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Konten Statis</label>
				<div class="col-md-9">
					<textarea class="form-control" id="static_content" name="static_content" ><?php echo set_value('static_content', $static_content->static_content); ?></textarea>
				<?php echo form_error('static_content'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('static_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="static_status" value="1" <?php echo set_value('static_status', ($static_content->static_status == '1') ? "checked" : ""); ?>> Terbit
					</label>
					<label>
						<input type="radio" name="static_status" value="0" <?php echo set_value('static_status', ($static_content->static_status === '0') ? "checked" : ""); ?>> Tidak Terbit
					</label>
				<?php echo form_error('static_status'); ?>
				</div>
			</div>
			

            <div class="form-group <?php echo (form_error('static_iscontactinfo') != "") ? "has-error" : "" ?>">
                                <label class="control-label col-md-3">Jenis</label>
                                <div class="col-md-3">
                                    <select class="form-control select2me" name="static_iscontactinfo" data-placeholder="Pilih..." >
                                        <option></option>
                                        <option value="1">Kontak</option>
                                        <option value="2">Bukan Kontak</option>
                                    </select>
                                    <?php echo form_error('static_iscontactinfo'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
    $(function() {
        CKEDITOR.replace('static_content');
    });
</script>