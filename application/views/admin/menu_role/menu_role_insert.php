<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Menu Role</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Menu Role
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Tambah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('role_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Role Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="role_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($role_list as $role){
						if($role->role_id == set_value('role_id',$menu_role->role_id)){
							echo "<option value='$role->role_id' selected>$role->role_id</option>";
						} else {
							echo "<option value='$role->role_id'>$role->role_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('role_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('menu_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Menu Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="menu_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($menu_list as $menu){
						if($menu->menu_id == set_value('menu_id',$menu_role->menu_id)){
							echo "<option value='$menu->menu_id' selected>$menu->menu_id</option>";
						} else {
							echo "<option value='$menu->menu_id'>$menu->menu_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('menu_id'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>