<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Informasi Website</h3>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data Informasi Website</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Judul:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->title; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Meta Judul:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->meta_title; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Meta Deskripsi:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->meta_description; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Alamat Judul:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->address_title; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Alamat:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->address; ?></p>
					</div>
				</div>
			</div>
			<!-- MAPS -->
			<div class="col-md-12">
			<?php if(!empty($website_info->address_latitude) && !empty($website_info->address_longitude)) {?>
			<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
			<div class="col-md-12">
				<div class="form-group">
						<label class="control-label col-md-2">Latitude, Longitude:</label>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group">
									<input name="longlat" type="text" value="" class="form-control" placeholder="Latitude, Longitude" readonly="true" />
									<input type="hidden" name="address_latitude" value="<?php echo set_value('address_latitude', $website_info->address_latitude); ?>" />
									<input type="hidden" name="address_longitude" value="<?php echo set_value('address_longitude', $website_info->address_longitude); ?>"/>
								</div>
								<div id="map_lokasi"></div>            
							</div>
						</div>
				</div>
			</div>
			</div>
			<?php } ?>
			<!-- END OF MAPS -->
			
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Telepon:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->phone; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Fax:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->fax; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Email:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->email; ?></p>
					</div>
				</div>
			</div>
<!-- 			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Live Tv:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->live_tv; ?></p>
					</div>
				</div>
			</div> -->
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Twitter:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->twitter; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Facebook:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->facebook; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Youtube:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $website_info->youtube; ?></p>
					</div>
				</div>
			</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $website_info->id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>