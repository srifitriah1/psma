<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Informasi Website</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="<?php echo base_url().$current_context; ?>">Informasi Website</a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Ubah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                    	<input type="hidden" name="id" value="<?php echo $website_info->id; ?>">
						<div class="form-group <?php echo (form_error('title') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Judul</label>
							<div class="col-md-9"><input class="form-control " name="title" value="<?php echo set_value('title', $website_info->title); ?>" placeholder="Judul"  maxlength=255>
							<?php echo form_error('title'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('meta_title') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Meta Judul</label>
							<div class="col-md-9"><input class="form-control " name="meta_title" value="<?php echo set_value('meta_title', $website_info->meta_title); ?>" placeholder="Meta Judul"  maxlength=255>
							<?php echo form_error('meta_title'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('meta_description') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Meta Deskripsi</label>
							<div class="col-md-9"><textarea class="form-control" name="meta_description" ><?php echo set_value('meta_description', $website_info->meta_description); ?></textarea>
							<?php echo form_error('meta_description'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('address_title') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Alamat Judul</label>
							<div class="col-md-9"><input class="form-control " name="address_title" value="<?php echo set_value('address_title', $website_info->address_title); ?>" placeholder="Alamat Judul"  maxlength=255>
							<?php echo form_error('address_title'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('address') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Alamat</label>
							<div class="col-md-9"><input class="form-control " name="address" value="<?php echo set_value('address', $website_info->address); ?>" placeholder="Alamat"  maxlength=255>
							<?php echo form_error('address'); ?>
							</div>
						</div>
						
						<!-- MAPS -->
						<script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
                        <div class="form-group" id="lokasi">
                            <label class="control-label col-md-3">Lokasi</label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="inputaddress" id="inputaddress" class="form-control" placeholder="Cari Lokasi"/>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input name="longlat" type="text" value="" class="form-control" placeholder="Latitude, Longitude" readonly="true" />
                                            <input type="hidden" name="address_latitude" value="<?php echo set_value('address_latitude', $website_info->address_latitude); ?>" />
                                            <input type="hidden" name="address_longitude" value="<?php echo set_value('address_longitude', $website_info->address_longitude); ?>"/>
                                            <span class="input-group-btn">
                                                <a class="btn btn-danger clearLat"><i class="fa fa-times"></i></a>
                                            </span>    
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="map_lokasi"></div>            
                                    </div>
                                </div>
                            </div>
                        </div>
						<!-- END OF MAPS -->
						
						<!--<div class="form-group <?php echo (form_error('address_latitude') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Alamat Latitude</label>
							<div class="col-md-9"><input class="form-control mask_decimal" name="address_latitude" value="<?php echo set_value('address_latitude', $website_info->address_latitude); ?>" placeholder="Alamat Latitude"  maxlength=12>
							<?php echo form_error('address_latitude'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('address_longitude') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Alamat Longitude</label>
							<div class="col-md-9"><input class="form-control mask_decimal" name="address_longitude" value="<?php echo set_value('address_longitude', $website_info->address_longitude); ?>" placeholder="Alamat Longitude"  maxlength=12>
							<?php echo form_error('address_longitude'); ?>
							</div>
						</div>-->
						<div class="form-group <?php echo (form_error('phone') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Telepon</label>
							<div class="col-md-9"><input class="form-control " name="phone" value="<?php echo set_value('phone', $website_info->phone); ?>" placeholder="Telepon"  maxlength=50>
							<?php echo form_error('phone'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('fax') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Fax</label>
							<div class="col-md-9"><input class="form-control " name="fax" value="<?php echo set_value('fax', $website_info->fax); ?>" placeholder="Fax"  maxlength=50>
							<?php echo form_error('fax'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('email') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Email</label>
							<div class="col-md-9"><input class="form-control " name="email" value="<?php echo set_value('email', $website_info->email); ?>" placeholder="Email"  maxlength=100>
							<?php echo form_error('email'); ?>
							</div>
						</div>
	<!-- 					<div class="form-group <?php echo (form_error('live_tv') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Live Tv</label>
							<div class="col-md-9"><input class="form-control " name="live_tv" value="<?php echo set_value('live_tv', $website_info->live_tv); ?>" placeholder="Live Tv"  maxlength=255>
							<?php echo form_error('live_tv'); ?>
							</div>
						</div> -->
						<div class="form-group <?php echo (form_error('twitter') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Twitter</label>
							<div class="col-md-9"><input class="form-control " name="twitter" value="<?php echo set_value('twitter', $website_info->twitter); ?>" placeholder="Twitter"  maxlength=100>
							<?php echo form_error('twitter'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('facebook') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Facebook</label>
							<div class="col-md-9"><input class="form-control " name="facebook" value="<?php echo set_value('facebook', $website_info->facebook); ?>" placeholder="Facebook"  maxlength=100>
							<?php echo form_error('facebook'); ?>
							</div>
						</div>
						<div class="form-group <?php echo (form_error('youtube') != "") ? "has-error" : "" ?>">
							<label class="control-label col-md-3">Youtube</label>
							<div class="col-md-9"><input class="form-control " name="youtube" value="<?php echo set_value('youtube', $website_info->youtube); ?>" placeholder="Youtube"  maxlength=255>
							<?php echo form_error('youtube'); ?>
							</div>
						</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>