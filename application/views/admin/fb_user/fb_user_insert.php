<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Anggota</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Anggota
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Tambah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('fb_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Fb Id</label>
				<div class="col-md-9"><input class="form-control " name="fb_id" value="<?php echo set_value('fb_id', $fb_user->fb_id); ?>" placeholder="Fb Id"  required  maxlength=50>
				<?php echo form_error('fb_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('fb_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Nama</label>
				<div class="col-md-9"><input class="form-control " name="fb_name" value="<?php echo set_value('fb_name', $fb_user->fb_name); ?>" placeholder="Nama"  required  maxlength=255>
				<?php echo form_error('fb_name'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('fb_isbanned') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9"><input class="form-control mask_number" name="fb_isbanned" value="<?php echo set_value('fb_isbanned', $fb_user->fb_isbanned); ?>" placeholder="Status"  required  maxlength=3>
				<?php echo form_error('fb_isbanned'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('fb_banreason') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Fb Banreason</label>
				<div class="col-md-9"><textarea class="form-control" name="fb_banreason" ><?php echo set_value('fb_banreason', $fb_user->fb_banreason); ?></textarea>
				<?php echo form_error('fb_banreason'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>