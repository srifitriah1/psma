<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Hak Akses</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Hak Akses
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data Hak Akses</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Nama:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $role->role_name; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Menu yang dapat diakses</label>
					<div class="col-md-10">
						<div class="table-responsive">
							<table class="table table-hover table-bordered">
								<thead>
								<th class="table-number">No</th>
								<th>Nama Menu</th>
								<th>Nama Submenu</th>
								<th>Nama Subsubmenu</th>
								</thead>
								<tbody>
									<?php
									if (empty($menu)) {
										?>
										<tr><td colspan="8">Tidak Ada Menu</td></tr>
										<?php
									} else {
										$i = 1;
										foreach ($menu as $parent) {
											?>
											<tr>
												<td><?php echo $i ?></td>
												<td><?php echo $parent->menu_name; ?></td>
												<?php
												if (!empty($parent->submenu)) {
													$j = 0;
													foreach ($parent->submenu as $child) {
														if ($j > 0) {
															$i++;
															echo "</tr><tr>" .
															"<td>" . $i . "</td>" .
															"<td>" . $parent->menu_name . "</td>";
														}
														?>
														<td><?php echo $child->menu_name; ?></td>
														<?php
														if (!empty($child->subsubmenu)) {
															$k = 0;
															foreach ($child->subsubmenu as $grandchild) {
//                                                                            print_r($grandchild);
																if ($k > 0) {
																	$i++;
																	echo "</tr><tr>" .
																	"<td>" . $i . "</td>" .
																	"<td>" . $parent->menu_name . "</td>" .
																	"<td>" . $child->menu_name . "</td>";
																}
																?>
																<td><?php echo $grandchild->menu_name; ?></td>
																<?php
																$k++;
															}
														} else {
															echo "<td></td>";
														}
														$j++;
													}
												} else {
													echo "<td></td><td></td>";
												}
												?>
											</tr>
											<?php
											$i++;
										}
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Status:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo ($role->role_status == 1)?"Aktif":"Tidak Aktif"; ?></p>
					</div>
				</div>
			</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $role->role_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>