<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Hak Akses</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Hak Akses
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('role_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Nama</label>
				<div class="col-md-9"><input class="form-control " name="role_name" value="<?php echo set_value('role_name', $role->role_name); ?>" placeholder="Nama"  required  maxlength=50>
				<?php echo form_error('role_name'); ?>
				</div>
			</div>
			
			<div class="form-group <?php echo (form_error('role_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="role_status" value="1" <?php echo set_value('role_status', ($role->role_status == '1') ? "checked" : ""); ?>> Aktif
					</label>
					<label>
						<input type="radio" name="role_status" value="0" <?php echo set_value('role_status', ($role->role_status === '0') ? "checked" : ""); ?>> Tidak Aktif
					</label>
				<?php echo form_error('role_status'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('menu') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Menu yang dapat diakses</label>
				<div class="col-md-9">
					<div class="col-md-10 checkbox-list">
						<?php
						if (!empty($menu)) {
							foreach ($menu as $parent) {
								$ac_menu = array_search($parent->menu_id, $active_menu);
								?>
								<ul class="list-unstyled parent-menu"><li><label><input type="checkbox" class="group-check" name="parent[]" value="<?php echo $parent->menu_id; ?>" <?php echo (($edit) ? ((!empty($ac_menu) || $ac_menu === 0) ? "checked" : "") : ""); ?>><b><?php echo $parent->menu_name; ?></b></label>
								<?php
								if (!empty($parent->submenu)) {
									echo "<ul class='list-unstyled child-menu'>";
									foreach ($parent->submenu as $child) {
										$ac_child = array_search($child->menu_id, $active_menu);
										?>
										<li><label><input type="checkbox" class="group-check-sec" name="child[]" value="<?php echo $child->menu_id; ?>" <?php echo (($edit) ? ((!empty($ac_child) || $ac_child === 0) ? "checked" : "") : "") ?>><?php echo $child->menu_name; ?></label>
										<?php
										if (!empty($child->subsubmenu)) {
											echo "<ul class='list-unstyled grand-menu'>";
											foreach ($child->subsubmenu as $grandchild) {
												$ac_grandchild = array_search($grandchild->menu_id, $active_menu);
												?>
											<li><label><input type="checkbox" class="check-menu" name="grandchild[]" value="<?php echo $grandchild->menu_id; ?>" <?php echo (($edit) ? ((!empty($ac_grandchild) || $ac_grandchild === 0) ? "checked" : "") : "") ?>><?php echo $grandchild->menu_name; ?></label></li>
												<?php
											} echo "</ul>";
										} echo "</li>";
									} echo "</ul>";
								} echo "</li></ul>";
							}
						}
						?>
					</div>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>