<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">PIP/BOS</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                PIP/BOS
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data PIP/BOS</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">Judul:</label>
									<div class="col-md-10">
										<p class="form-control-static"><?php echo $bos->bos_title; ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">Penulis:</label>
									<div class="col-md-10">
										<p class="form-control-static"><?php echo $bos->user_fullname; ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">Ringkasan:</label>
									<div class="col-md-10">
										<p class="form-control-static"><?php echo $bos->bos_summary; ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">Isi Berita:</label>
									<div class="col-md-10">
										<p class="form-control-static"><?php echo $bos->bos_content; ?></p>
									</div>
								</div>
							</div>
<!-- 							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">Quotes:</label>
									<div class="col-md-10">
										<p class="form-control-static"><?php echo $bos->bos_quotes; ?></p>
									</div>
								</div>
							</div> -->
							<?php if(!empty($bos->bos_photo)){?>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">Gambar Thumbnail:</label>
									<div class="col-md-10">
										<p class="form-control-static"><img src="<?php echo base_url().$bos->bos_photo; ?>" width=200px></p>
									</div>
								</div>
							</div>
							<?php } ?>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">Tanggal Terbit:</label>
									<div class="col-md-10">
										<p class="form-control-static"><?php echo $this->public_function->format_date($bos->bos_posted_on); ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label class="control-label col-md-2">PIP/BOS Status:</label>
									<div class="col-md-10">
										<p class="form-control-static"><?php echo ($bos->bos_status == 1)?"Terbit":"Tidak Terbit"; ?></p>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $bos->bos_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>