<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Testimonial</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Testimonial
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('testimonial_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Nama</label>
				<div class="col-md-9"><input class="form-control " name="testimonial_name" value="<?php echo set_value('testimonial_name', $testimonial->testimonial_name); ?>" placeholder="Nama"  required  maxlength=100>
				<?php echo form_error('testimonial_name'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('testimonial_photo') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Foto</label>
				<div class="col-md-9">
				<?php
					if ($edit) {
						echo (empty($testimonial->testimonial_photo)) ? "" : "<img src='" . base_url() . $testimonial->testimonial_photo . "' width=200px>";
					}
					?>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="input-group">
							<span class="input-group-btn">
								<span class="uneditable-input">
									<i class="fa fa-file fileupload-exists"></i> 
									<span class="fileupload-preview"></span>
								</span>
							</span>
							<span class="btn default btn-file">
								<span class="fileupload-new"><i class="fa fa-paperclip"></i> Pilih File</span>
								<span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah File</span>
								<input type="file" class="default" name="testimonial_photo" placeholder="File" accept="image/*"/>
							</span>
							<a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Hapus</a>
						</div>
					</div>
					<?php echo ($edit && !empty($testimonial->testimonial_photo)) ? '<p class="help-block">Masukkan file baru jika ingin mengubah.</p>' : '' ?>
					<?php echo form_error('testimonial_photo'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('testimonial_content') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Konten</label>
				<div class="col-md-9"><textarea class="form-control" name="testimonial_content" ><?php echo set_value('testimonial_content', $testimonial->testimonial_content); ?></textarea>
				<?php echo form_error('testimonial_content'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('testimonial_date') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Tanggal</label>
				<div class="col-md-3"><div class="input-group date date-picker" data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
				<input type="text" name="testimonial_date" class="form-control" readonly value="<?php echo set_value('testimonial_date', $testimonial->testimonial_date); ?>" placeholder="Tanggal">
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
				<?php echo form_error('testimonial_date'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('testimonial_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="testimonial_status" value="1" <?php echo set_value('testimonial_status', ($testimonial->testimonial_status == '1') ? "checked" : ""); ?>> Aktif
					</label>
					<label>
						<input type="radio" name="testimonial_status" value="0" <?php echo set_value('testimonial_status', ($testimonial->testimonial_status === '0') ? "checked" : ""); ?>> Tidak Aktif
					</label>
				<?php echo form_error('testimonial_status'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>