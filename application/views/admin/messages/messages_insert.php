<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Pesan</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Pesan
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('message_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Nama</label>
				<div class="col-md-9"><input class="form-control " name="message_name" value="<?php echo set_value('message_name', $messages->message_name); ?>" placeholder="Nama"  required  maxlength=50>
				<?php echo form_error('message_name'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('message_email') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-9"><input class="form-control " name="message_email" value="<?php echo set_value('message_email', $messages->message_email); ?>" placeholder="Email"  required  maxlength=100>
				<?php echo form_error('message_email'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('message_title') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Judul</label>
				<div class="col-md-9"><input class="form-control " name="message_title" value="<?php echo set_value('message_title', $messages->message_title); ?>" placeholder="Judul"  maxlength=255>
				<?php echo form_error('message_title'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('message_content') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Isi</label>
				<div class="col-md-9"><textarea class="form-control" name="message_content" ><?php echo set_value('message_content', $messages->message_content); ?></textarea>
				<?php echo form_error('message_content'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('message_date') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Tanggal</label>
				<div class="col-md-3"><div class="input-group date date-picker" data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
				<input type="text" name="message_date" class="form-control" readonly value="<?php echo set_value('message_date', $messages->message_date); ?>" placeholder="Tanggal">
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
				<?php echo form_error('message_date'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('message_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="message_status" value="1" <?php echo set_value('message_status', ($messages->message_status == '1') ? "checked" : ""); ?>> Sudah Dibaca
					</label>
					<label>
						<input type="radio" name="message_status" value="0" <?php echo set_value('message_status', ($messages->message_status === '0') ? "checked" : ""); ?>> Belum Dibaca
					</label>
				<?php echo form_error('message_status'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>