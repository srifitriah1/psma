<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Pengguna</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Pengguna
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data Pengguna</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Hak Akses:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $users->role_name; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Nama Pengguna:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $users->user_fullname; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Username:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $users->user_username; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Email:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $users->user_email; ?></p>
					</div>
				</div>
			</div>
			<?php if(!empty($users->user_photo)){?>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Foto:</label>
					<div class="col-md-10">
						<p class="form-control-static"><img src="<?php echo base_url().$users->user_photo; ?>" width=200px></p>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Tanggal Registrasi:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $this->public_function->format_date($users->user_registered_on); ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Status:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo ($users->user_status == 1)?"Aktif":"Tidak Aktif"; ?></p>
					</div>
				</div>
			</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $users->user_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>