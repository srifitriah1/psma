<!DOCTYPE html>
<html lang="en">
<head>
   <meta chrset="utf-8" />
   <title>ZEEALS SHOP ONLINE</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" name="viewport" />

   <!-- Kebudayaan Indonesia Style -->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/frontstyle.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/fontawesome/css/font-awesome.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/font3.css">
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico" type="image/x-icon" />
</head>
<body>
   <header>
      <nav class="navbar navbar-default">
        <div class="container-fluid" style="height:90px">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <!-- <a class="navbar-brand" href="<?php echo base_url() ?>">
               <img src="<?php echo base_url() ?>assets/img/logo.png" class="img-responsive">               
            </a> -->
            <h3 style="color: white; margin-top: 14%;">ZEEALS SHOP ONLINE</h3>
          </div>
        </div><!-- /.container-fluid -->
      </nav>
   </header>
  
   <section id="login-section">
     <div class="container" style="margin: 50px auto 50px;">
       <div class="row"><br>
		<?php
			$message = $this->session->flashdata('message');
			$type_message = $this->session->flashdata('type_message');
			echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-6 col-md-offset-3" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
			echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-6 col-md-offset-3" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
		?>
         <div class="col-md-6 col-md-offset-3">
         <h1>Login</h1>
			<form name="login"  method="POST" action="<?php echo base_url(); ?>admin/login/auth" role="form">
              <div class="form-group">
                <label for="username" class="control-label">Username</label>
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-user"></i></div>
                  <input class="form-control" type="text" name="username" id="username" />
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="control-label">Password</label>
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-key"></i></div>
                  <input class="form-control" type="password" name="password" id="password" />
                </div>
              </div>
              <div class="form-group">
                <a href="" data-toggle="modal" data-target="#forgotPassword">Forget Password?</a>
              </div>
			  <div class="modal-footer">
				  <button type="submit" class="btn btn-primary">Login</button>
				  <a href=""><button type="submit" class="btn btn-default">Register</button></a>
			  </div>
            </form>
         </div>
       </div>
     </div>
   </section>
   <footer>
      <div class="container" style="padding-top: 50px; padding-bottom: 50px;">
         <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <span class="pull-right" style="color: #777;padding-left: 50px;">&copy; 2015 Direktorat Jenderal Kebudayaan Republik Indonesia</span>
            </div>
         </div>
      </div>
   </footer>
   <div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
      <form name="forgotPassword"  method="POST" action="<?php echo base_url(); ?>admin/login/forgot_password" role="form">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-sign-in fa-fw"></i>&nbsp; Forgot Password</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
              <label for="email" class="control-label">Email</label>
              <div class="input-group">
                <input class="form-control" type="email" name="email" id="email" />
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <!-- <a href=""><button type="button" class="btn btn-default">Daftar</button></a> -->
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</body>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
   <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
</html>