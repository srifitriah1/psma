<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Polling</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Polling
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Tambah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('polling_author') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Pembuat</label>
				<div class="col-md-9"><select class="form-control select2me" name="polling_author" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($users_list as $users){
						if($users->user_id == set_value('polling_author',$polling->polling_author)){
							echo "<option value='$users->user_id' selected>$users->user_fullname</option>";
						} else {
							echo "<option value='$users->user_id'>$users->user_fullname</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('polling_author'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('polling_title') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Judul</label>
				<div class="col-md-9"><input class="form-control " name="polling_title" value="<?php echo set_value('polling_title', $polling->polling_title); ?>" placeholder="Judul"  required  maxlength=100>
				<?php echo form_error('polling_title'); ?>
				</div>
			</div>
			
			<div class="form-group <?php echo (form_error('polling_image') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Gambar</label>
				<div class="col-md-9">
				<?php
					if ($edit) {
						echo (empty($polling->polling_image)) ? "" : "<img src='" . base_url() . $polling->polling_image . "' width=200px>";
					}
					?>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="input-group">
							<span class="input-group-btn">
								<span class="uneditable-input">
									<i class="fa fa-file fileupload-exists"></i> 
									<span class="fileupload-preview"></span>
								</span>
							</span>
							<span class="btn default btn-file">
								<span class="fileupload-new"><i class="fa fa-paperclip"></i> Pilih File</span>
								<span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah File</span>
								<input type="file" class="default" name="polling_image" placeholder="File" accept="image/*"/>
							</span>
							<a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Hapus</a>
						</div>
					</div>
					<?php echo ($edit && !empty($polling->polling_image)) ? '<p class="help-block">Masukkan file baru jika ingin mengubah.</p>' : '' ?>
					<?php echo form_error('polling_image'); ?>
				</div>
			</div>
			
			<div class="form-group <?php echo (form_error('polling_description') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Deskripsi</label>
				<div class="col-md-9"><textarea class="form-control" name="polling_description" ><?php echo set_value('polling_description', $polling->polling_description); ?></textarea>
				<?php echo form_error('polling_description'); ?>
				</div>
			</div>
			<!--<div class="form-group <?php echo (form_error('polling_posted_on') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Tanggal</label>
				<div class="col-md-3"><div class="input-group date date-picker" data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
				<input type="text" name="polling_posted_on" class="form-control" readonly value="<?php echo set_value('polling_posted_on', $polling->polling_posted_on); ?>" placeholder="Tanggal">
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
				<?php echo form_error('polling_posted_on'); ?>
				</div>
			</div>-->
			<div class="form-group <?php echo (form_error('polling_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="polling_status" value="1" <?php echo set_value('polling_status', ($polling->polling_status == '1') ? "checked" : ""); ?>> Aktif
					</label>
					<label>
						<input type="radio" name="polling_status" value="0" <?php echo set_value('polling_status', ($polling->polling_status === '0') ? "checked" : ""); ?>> Tidak Aktif
					</label>
				<?php echo form_error('polling_status'); ?>
				</div>
			</div>
			
			<div class="wrapper-add-polling">
				<div class="form-group">
					<div class="col-md-12">
						<h3 class="pull-left">Sub Polling</h3><a onclick="addinputpolling()" style="margin-top:10px;" class="btn green add-input-block pull-right"><span class="fa fa-plus"></span></a>
					</div>
				</div>
				<!--ADD-->
				<?php
				
				if (empty($polling_list)) {
					?>
					<div>
						<div class="add-block">
							<div class="form-group <?php echo (form_error('sub_title') != "") ? "has-error" : "" ?>">
								<label class="control-label col-md-2">Judul Sub Polling :</label>
								<div class="col-md-10"><input class="form-control" name="sub_title[0]" placeholder="Judul Sub Polling" required maxlength=100>
									<?php echo form_error('sub_title'); ?>
								</div>
							</div>
							<div class="form-group <?php echo (form_error('sub_choice1') != "") ? "has-error" : "" ?>">
								<label class="control-label col-md-2">Pilihan 1</label>
								<div class="col-md-10"><input class="form-control " name="sub_choice1[0]" placeholder="Pilihan 1"  required  maxlength=100>
								<?php echo form_error('sub_choice1'); ?>
								</div>
							</div>
							<div class="form-group <?php echo (form_error('sub_choice2') != "") ? "has-error" : "" ?>">
								<label class="control-label col-md-2">Pilihan 2</label>
								<div class="col-md-10"><input class="form-control " name="sub_choice2[0]" placeholder="Pilihan 2"  required  maxlength=100>
								<?php echo form_error('sub_choice2'); ?>
								</div>
							</div>
							<div class="form-group <?php echo (form_error('sub_choice3') != "") ? "has-error" : "" ?>">
								<label class="control-label col-md-2">Pilihan 3</label>
								<div class="col-md-10"><input class="form-control " name="sub_choice3[0]" placeholder="Pilihan 3"  maxlength=100>
								<?php echo form_error('sub_choice3'); ?>
								</div>
							</div>
							<div class="form-group <?php echo (form_error('sub_choice4') != "") ? "has-error" : "" ?>">
								<label class="control-label col-md-2">Pilihan 4</label>
								<div class="col-md-10"><input class="form-control " name="sub_choice4[0]" placeholder="Pilihan 4"  maxlength=100>
								<?php echo form_error('sub_choice4'); ?>
								</div>
							</div>
							<div class="form-group <?php echo (form_error('sub_choice5') != "") ? "has-error" : "" ?>">
								<label class="control-label col-md-2">Pilihan 5</label>
								<div class="col-md-10"><input class="form-control " name="sub_choice5[0]" placeholder="Pilihan 5"  maxlength=100>
								<?php echo form_error('sub_choice5'); ?>
								</div>
							</div>
							<div class="form-group <?php echo (form_error('sub_choice6') != "") ? "has-error" : "" ?>">
								<label class="control-label col-md-2">Pilihan 6</label>
								<div class="col-md-10"><input class="form-control " name="sub_choice6[0]" placeholder="Pilihan 6"  maxlength=100>
								<?php echo form_error('sub_choice6'); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<a class='btn red remove_selected_block pull-right'><span class='fa fa-times'></span></a>
								</div>
							</div>
							<div class="form-group">
								<h6></h6>
							</div>
						</div>
					</div>
				<!--ADD-->
				<!--EDIT-->
					<?php
				} else {
					$i = 0;
						foreach ($polling_list as $sub) {
							if($sub->poll_id == $polling->polling_id){
							?>	
							<div>
								<div class="add-block">
									<div class="form-group <?php echo (form_error('sub_title') != "") ? "has-error" : "" ?>">
										<label class="control-label col-md-2">Judul Sub Polling :</label>
										<div class="col-md-10">
											<input class="form-control" name="sub_title[<?php echo $i; ?>]" value="<?php echo set_value('sub_title', $sub->sub_title); ?>" placeholder="Judul Sub Polling" required maxlength=100>
											<?php echo form_error('sub_title'); ?>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('sub_choice1') != "") ? "has-error" : "" ?>">
										<label class="control-label col-md-2">Pilihan 1</label>
										<div class="col-md-10"><input class="form-control " name="sub_choice1[<?php echo $i; ?>]" value="<?php echo set_value('sub_choice1', $sub->sub_choice1); ?>" placeholder="Pilihan 1"  required  maxlength=100>
										<?php echo form_error('sub_choice1'); ?>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('sub_choice2') != "") ? "has-error" : "" ?>">
										<label class="control-label col-md-2">Pilihan 2</label>
										<div class="col-md-10"><input class="form-control " name="sub_choice2[<?php echo $i; ?>]" value="<?php echo set_value('sub_choice2', $sub->sub_choice2); ?>" placeholder="Pilihan 2"  required  maxlength=100>
										<?php echo form_error('sub_choice2'); ?>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('sub_choice3') != "") ? "has-error" : "" ?>">
										<label class="control-label col-md-2">Pilihan 3</label>
										<div class="col-md-10"><input class="form-control " name="sub_choice3[<?php echo $i; ?>]" value="<?php echo set_value('sub_choice3', $sub->sub_choice3); ?>" placeholder="Pilihan 3"  maxlength=100>
										<?php echo form_error('sub_choice3'); ?>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('sub_choice4') != "") ? "has-error" : "" ?>">
										<label class="control-label col-md-2">Pilihan 4</label>
										<div class="col-md-10"><input class="form-control " name="sub_choice4[<?php echo $i; ?>]" value="<?php echo set_value('sub_choice4', $sub->sub_choice4); ?>" placeholder="Pilihan 4"  maxlength=100>
										<?php echo form_error('sub_choice4'); ?>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('sub_choice5') != "") ? "has-error" : "" ?>">
										<label class="control-label col-md-2">Pilihan 5</label>
										<div class="col-md-10"><input class="form-control " name="sub_choice5[<?php echo $i; ?>]" value="<?php echo set_value('sub_choice5', $sub->sub_choice5); ?>" placeholder="Pilihan 5"  maxlength=100>
										<?php echo form_error('sub_choice5'); ?>
										</div>
									</div>
									<div class="form-group <?php echo (form_error('sub_choice6') != "") ? "has-error" : "" ?>">
										<label class="control-label col-md-2">Pilihan 6</label>
										<div class="col-md-10"><input class="form-control " name="sub_choice6[<?php echo $i; ?>]" value="<?php echo set_value('sub_choice6', $sub->sub_choice6); ?>" placeholder="Pilihan 6"  maxlength=100>
										<?php echo form_error('sub_choice6'); ?>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<a class='btn red remove_selected_block pull-right'><span class='fa fa-times'></span></a>
									</div>
								</div>
							</div>
							<!--EDIT-->
							<?php
							$i++;
						}
					}
				}
				?>
			</div>
			
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var countpolling = <?php echo (empty($polling_list)) ? 1 : count($polling_list); ?>;
    var countjudul = countpolling + 1;
	var counttubuh = <?php echo (empty($tubuh_manusia)) ? 1 : count($tubuh_manusia); ?>;
    var countjudultubuh = counttubuh + 1;
    function addinputpolling() {
        $('.wrapper-add-polling').append(
                '<div class="add-block">'+
                '<div class="form-group <?php echo (form_error("sub_title") != "") ? "has-error" : "" ?>">' +
                '<label class="control-label col-md-2">Judul Sub Polling</label>' +
                '<div class="col-md-10"><input class="form-control" name="sub_title[' + countpolling + ']" value="<?php //echo set_value("sub_title", $sistah_sistem_pengetahuan->sub_title);  ?>" placeholder="Judul Sub Polling" required maxlength=100>' +
                '<?php echo form_error("sub_title"); ?>' +
                '</div>' +
                '</div>' +
                '<div class="form-group <?php echo (form_error('sub_choice1') != "") ? "has-error" : "" ?>">' +
					'<label class="control-label col-md-2">Pilihan 1</label>' +
					'<div class="col-md-10"><input class="form-control " name="sub_choice1[' + countpolling + ']" placeholder="Pilihan 1"  required  maxlength=100>' +
					'<?php echo form_error('sub_choice1'); ?>' +
					'</div>' +
				'</div>' +
				'<div class="form-group <?php echo (form_error('sub_choice2') != "") ? "has-error" : "" ?>">' +
					'<label class="control-label col-md-2">Pilihan 2</label>' +
					'<div class="col-md-10"><input class="form-control " name="sub_choice2[' + countpolling + ']" placeholder="Pilihan 2"  required  maxlength=100>' +
					'<?php echo form_error('sub_choice2'); ?>' +
					'</div>' +
				'</div>' +
				'<div class="form-group <?php echo (form_error('sub_choice3') != "") ? "has-error" : "" ?>">' +
					'<label class="control-label col-md-2">Pilihan 3</label>' +
					'<div class="col-md-10"><input class="form-control " name="sub_choice3[' + countpolling + ']" placeholder="Pilihan 3"  maxlength=100>' +
					'<?php echo form_error('sub_choice3'); ?>' +
					'</div>' +
				'</div>' +
				'<div class="form-group <?php echo (form_error('sub_choice4') != "") ? "has-error" : "" ?>">' +
					'<label class="control-label col-md-2">Pilihan 4</label>' +
					'<div class="col-md-10"><input class="form-control " name="sub_choice4[' + countpolling + ']" placeholder="Pilihan 4"  maxlength=100>' +
					'<?php echo form_error('sub_choice4'); ?>' +
					'</div>' +
				'</div>' +
				'<div class="form-group <?php echo (form_error('sub_choice5') != "") ? "has-error" : "" ?>">' +
					'<label class="control-label col-md-2">Pilihan 5</label>' +
					'<div class="col-md-10"><input class="form-control " name="sub_choice5[' + countpolling + ']" placeholder="Pilihan 5"  maxlength=100>' +
					'<?php echo form_error('sub_choice5'); ?>' +
					'</div>' +
				'</div>' +
				'<div class="form-group <?php echo (form_error('sub_choice6') != "") ? "has-error" : "" ?>">' +
					'<label class="control-label col-md-2">Pilihan 6</label>' +
					'<div class="col-md-10"><input class="form-control " name="sub_choice6[' + countpolling + ']" placeholder="Pilihan 6"  maxlength=100>' +
					'<?php echo form_error('sub_choice6'); ?>' +
					'</div>' +
				'</div>' +
				'<div class="form-group">'+
				'<div class="col-md-12">' +
                '<a class="btn red remove_selected_block pull-right"><span class="fa fa-times"></span></a>' +
                '</div>' +
                '</div>' +
                '<div class="form-group">' +
                '<h6></h6>' +
                '</div></div>');
        countpolling += 1;
        countjudul += 1;
    }
	
	function addgambar(element) {
        var input = $(element).closest('table').find('.input-file').html();
        var button = "<a style='margin-top: -9px;' class='btn red remove_selected'><span class='fa fa-minus'></span></a>";
        $(element).parents('table').append("<tr><td>" + button + "</td><td>" + input + "</td></tr>");
    }
</script>