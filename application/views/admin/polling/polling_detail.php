<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Polling</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Polling
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data Polling</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Pembuat:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $polling->user_fullname; ?></p>
					</div>
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Judul:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $polling->polling_title; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Gambar:</label>
					<div class="col-md-10">
						<p class="form-control-static">
							<img src="<?php echo base_url().$polling->polling_image; ?>" width=200px>
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Deskripsi:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $polling->polling_description; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Tanggal:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $polling->polling_posted_on; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Status:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo ($polling->polling_status==1)?"Aktif":"Tidak Aktif"; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<h3 class="pull-left">Sub Polling</h3><a style="margin-top:10px;"></a>
			</div>
			<?php 
			if (empty($sub)) {
				?>
				<tr><td colspan="12">Tidak Ada Data</td></tr>
				<?php
			} else {
				foreach($sub as $polling_sub){
				?>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-2">Judul:</label>
							<div class="col-md-10">
								<p class="form-control-static"><?php echo $polling_sub->sub_title; ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Pilihan 1:</label>
							<div class="col-md-9">
								<p class="form-control-static"><?php echo $polling_sub->sub_choice1; ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Pilihan 2:</label>
							<div class="col-md-9">
								<p class="form-control-static"><?php echo $polling_sub->sub_choice2; ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Pilihan 3:</label>
							<div class="col-md-9">
								<p class="form-control-static"><?php echo $polling_sub->sub_choice3; ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Pilihan 4:</label>
							<div class="col-md-9">
								<p class="form-control-static"><?php echo $polling_sub->sub_choice4; ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Pilihan 5:</label>
							<div class="col-md-9">
								<p class="form-control-static"><?php echo $polling_sub->sub_choice5; ?></p>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Pilihan 6:</label>
							<div class="col-md-9">
								<p class="form-control-static"><?php echo $polling_sub->sub_choice6; ?></p>
							</div>
						</div>
					</div>
				<?php 
					}	
				}
				?>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $polling->polling_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>