<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Struktur Organisasi</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Struktur Organisasi
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data Struktur Organisasi</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Penulis:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->user_fullname; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Nama:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->struktur_name; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Posisi:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->struktur_position; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Deskripsi:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->struktur_desc; ?></p>
					</div>
				</div>
			</div>
			<?php if(!empty($struktur_organisasi->struktur_photo)){?>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Foto:</label>
					<div class="col-md-10">
						<p class="form-control-static"><img src="<?php echo base_url().$struktur_organisasi->struktur_photo; ?>" width=200px></p>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Telepon:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->struktur_phone; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Email:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->struktur_email; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Facebook:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->struktur_facebook; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Twitter:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $struktur_organisasi->struktur_twitter; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Status:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo ($struktur_organisasi->struktur_status == 1)?"Aktif":"Tidak Aktif"; ?></p>
					</div>
				</div>
			</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $struktur_organisasi->struktur_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>