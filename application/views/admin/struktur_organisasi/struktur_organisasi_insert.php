<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Struktur Organisasi</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Struktur Organisasi
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('struktur_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Nama</label>
				<div class="col-md-9"><input class="form-control " name="struktur_name" value="<?php echo set_value('struktur_name', $struktur_organisasi->struktur_name); ?>" placeholder="Nama"  required  maxlength=100>
				<?php echo form_error('struktur_name'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_position') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Posisi</label>
				<div class="col-md-9"><input class="form-control " name="struktur_position" value="<?php echo set_value('struktur_position', $struktur_organisasi->struktur_position); ?>" placeholder="Posisi"  required  maxlength=100>
				<?php echo form_error('struktur_position'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_desc') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Deskripsi</label>
				<div class="col-md-9"><textarea class="form-control" name="struktur_desc" ><?php echo set_value('struktur_desc', $struktur_organisasi->struktur_desc); ?></textarea>
				<?php echo form_error('struktur_desc'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_photo') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Foto</label>
				<div class="col-md-9">
				<?php
					if ($edit) {
						echo (empty($struktur_organisasi->struktur_photo)) ? "" : "<img src='" . base_url() . $struktur_organisasi->struktur_photo . "' width=200px>";
					}
					?>
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="input-group">
							<span class="input-group-btn">
								<span class="uneditable-input">
									<i class="fa fa-file fileupload-exists"></i> 
									<span class="fileupload-preview"></span>
								</span>
							</span>
							<span class="btn default btn-file">
								<span class="fileupload-new"><i class="fa fa-paperclip"></i> Pilih File</span>
								<span class="fileupload-exists"><i class="fa fa-undo"></i> Ubah File</span>
								<input type="file" class="default" name="struktur_photo" placeholder="File" accept="image/*"/>
							</span>
							<a href="#" class="btn red fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Hapus</a>
						</div>
					</div>
					<?php echo ($edit && !empty($struktur_organisasi->struktur_photo)) ? '<p class="help-block">Masukkan file baru jika ingin mengubah.</p>' : '' ?>
					<?php echo form_error('struktur_photo'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_phone') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Telepon</label>
				<div class="col-md-9"><input class="form-control mask-number" name="struktur_phone" value="<?php echo set_value('struktur_phone', $struktur_organisasi->struktur_phone); ?>" placeholder="Telepon"  maxlength=50>
				<?php echo form_error('struktur_phone'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_email') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Email</label>
				<div class="col-md-9"><input class="form-control " name="struktur_email" value="<?php echo set_value('struktur_email', $struktur_organisasi->struktur_email); ?>" placeholder="Email"  maxlength=100>
				<?php echo form_error('struktur_email'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_facebook') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Facebook</label>
				<div class="col-md-9"><input class="form-control " name="struktur_facebook" value="<?php echo set_value('struktur_facebook', $struktur_organisasi->struktur_facebook); ?>" placeholder="Facebook"  maxlength=150>
				<?php echo form_error('struktur_facebook'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_twitter') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Twitter</label>
				<div class="col-md-9"><input class="form-control " name="struktur_twitter" value="<?php echo set_value('struktur_twitter', $struktur_organisasi->struktur_twitter); ?>" placeholder="Twitter"  maxlength=150>
				<?php echo form_error('struktur_twitter'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('struktur_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="struktur_status" value="1" <?php echo set_value('struktur_status', ($struktur_organisasi->struktur_status == '1') ? "checked" : ""); ?>> Aktif
					</label>
					<label>
						<input type="radio" name="struktur_status" value="0" <?php echo set_value('struktur_status', ($struktur_organisasi->struktur_status === '0') ? "checked" : ""); ?>> Tidak Aktif
					</label>
				<?php echo form_error('struktur_status'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>