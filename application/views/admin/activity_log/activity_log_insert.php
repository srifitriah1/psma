<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Activity Log</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Activity Log
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Tambah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('news_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">News Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="news_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($news_list as $news){
						if($news->news_id == set_value('news_id',$activity_log->news_id)){
							echo "<option value='$news->news_id' selected>$news->news_id</option>";
						} else {
							echo "<option value='$news->news_id'>$news->news_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('news_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('storg_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Storg Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="storg_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($struktur_organisasi_list as $struktur_organisasi){
						if($struktur_organisasi->struktur_id == set_value('storg_id',$activity_log->storg_id)){
							echo "<option value='$struktur_organisasi->struktur_id' selected>$struktur_organisasi->struktur_id</option>";
						} else {
							echo "<option value='$struktur_organisasi->struktur_id'>$struktur_organisasi->struktur_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('storg_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('user_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">User Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="user_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($users_list as $users){
						if($users->user_id == set_value('user_id',$activity_log->user_id)){
							echo "<option value='$users->user_id' selected>$users->user_id</option>";
						} else {
							echo "<option value='$users->user_id'>$users->user_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('user_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('static_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Static Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="static_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($static_content_list as $static_content){
						if($static_content->static_id == set_value('static_id',$activity_log->static_id)){
							echo "<option value='$static_content->static_id' selected>$static_content->static_id</option>";
						} else {
							echo "<option value='$static_content->static_id'>$static_content->static_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('static_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('log_desc') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Log Desc</label>
				<div class="col-md-9"><textarea class="form-control" name="log_desc" ><?php echo set_value('log_desc', $activity_log->log_desc); ?></textarea>
				<?php echo form_error('log_desc'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('log_type') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Log Type</label>
				<div class="col-md-9"><input class="form-control " name="log_type" value="<?php echo set_value('log_type', $activity_log->log_type); ?>" placeholder="Log Type"  required  maxlength=50>
				<?php echo form_error('log_type'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('log_datetime') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Log Datetime</label>
				<div class="col-md-9"><div class="input-group date date-picker" data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
				<input type="text" name="log_datetime" class="form-control" readonly value="<?php echo set_value('log_datetime', $activity_log->log_datetime); ?>" placeholder="Log Datetime">
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
				<?php echo form_error('log_datetime'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('log_before') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Log Before</label>
				<div class="col-md-9"><textarea class="form-control" name="log_before" ><?php echo set_value('log_before', $activity_log->log_before); ?></textarea>
				<?php echo form_error('log_before'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('log_after') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Log After</label>
				<div class="col-md-9"><textarea class="form-control" name="log_after" ><?php echo set_value('log_after', $activity_log->log_after); ?></textarea>
				<?php echo form_error('log_after'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>