<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Menu</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Menu
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
						
                        <?php 
						// echo"<pre>";
						// print_r($menu_list);die();
						?>
			<div class="form-group <?php echo (form_error('parent_menu_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Parent</label>
				<div class="col-md-9"><select class="form-control select2me" name="parent_menu_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($menu_list as $ml){
							if($ml->menu_id == set_value('parent_menu_id',$menu->parent_menu_id)){
								echo "<option value='$ml->menu_id' selected>$ml->menu_name</option>";
							} else {
								echo "<option value='$ml->menu_id'>$ml->menu_name</option>";
							}
					}
					?>
				</select>
				<?php echo form_error('parent_menu_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('menu_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Nama Menu</label>
				<div class="col-md-9"><input class="form-control " name="menu_name" value="<?php echo set_value('menu_name', $menu->menu_name); ?>" placeholder="Nama Menu"  required  maxlength=50>
				<?php echo form_error('menu_name'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('menu_link') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Link Menu</label>
				<div class="col-md-9"><input class="form-control " name="menu_link" value="<?php echo set_value('menu_link', $menu->menu_link); ?>" placeholder="Link Menu"  required  maxlength=150>
				<?php echo form_error('menu_link'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('menu_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="menu_status" value="1" <?php echo set_value('menu_status', ($menu->menu_status == '1') ? "checked" : ""); ?>> Aktif
					</label>
					<label>
						<input type="radio" name="menu_status" value="0" <?php echo set_value('menu_status', ($menu->menu_status === '0') ? "checked" : ""); ?>> Tidak Aktif
					</label>
				<?php echo form_error('menu_status'); ?>
				</div>
			</div>
			<!--<div class="form-group <?php echo (form_error('menu_isparent') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Menu Isparent</label>
				<div class="col-md-9"><input class="form-control mask_number" name="menu_isparent" value="<?php echo set_value('menu_isparent', $menu->menu_isparent); ?>" placeholder="Menu Isparent"  required  maxlength=3>
				<?php echo form_error('menu_isparent'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('menu_order') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Urutan Menu</label>
				<div class="col-md-9"><input class="form-control mask_number" name="menu_order" value="<?php echo set_value('menu_order', $menu->menu_order); ?>" placeholder="Urutan Menu"  maxlength=10>
				<?php echo form_error('menu_order'); ?>
				</div>
			</div>-->
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>