<!-- BEGIN PAGE HEADER-->   
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Menu</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="<?php echo base_url(); ?>">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>Menu</li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-search fa-fw"></i>&nbsp; Pencarian</div>
            </div>
            <div class="portlet-body">
                <form method="post" action="<?php echo $current_context . 'search'; ?>" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $key = (object) $this->session->userdata('filter_menu'); ?>
						<div class="col-md-6"><div class="form-group">
								<label class="control-label col-md-5">Nama Menu</label>
								<div class="col-md-7"><input class="form-control " name="menu_name" value="<?php echo $key->menu_name; ?>" placeholder="Nama Menu">
								</div>
							</div>
						</div>
                            <input type="hidden" id="search" name="search" value="true">
                            <div class="clearfix">
                                <button type="submit" class="btn green pull-right">Lakukan Pencarian</button>
                                <button type="button" class="btn default pull-right" onclick="location.href='<?php echo $current_context; ?>'">Kosongkan Pencarian</button>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
<?php
	$message = $this->session->flashdata('message');
	$type_message = $this->session->flashdata('type_message');
	echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
	echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-table fa-fw"></i>&nbsp; Tabel Data (<?php echo $total_rows;?> Data)</div> <!-- SHOW TITLE AND NUMBER OF DATA -->
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="table_data">
                    <thead>
                        <tr>
                        <th class="table-checkbox"><input type="checkbox" class="group-checkable" data-set="#table_data .checkboxes" /></th>
                            <th>No</th>
    						<th>Parent</th>
    						<th>Nama Menu</th>
    						<th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i=1;
                        foreach ($menu as $row) {
                            ?>
                            <tr class="odd gradeX">
                                <td><input type="checkbox" class="checkboxes"  data-menu_id="<?php echo $row->menu_id; ?>"  /></td>
                                <td><?php echo $i + $offset; ?></td>
    							<td><?php echo $row->parent_menu_name; ?></td>
    							<td><?php echo $row->menu_name; ?></td>
    							<td><?php echo ($row->menu_status==='0')?"Tidak Aktif":"Aktif"; ?></td>
							<td>
								<a href="<?php echo $current_context . 'edit'  .'/'. $row->menu_id ?>" class="btn blue btn-xs"><i class="fa fa-edit fa-fw"></i>ubah</a>
							</td>
                            </tr>
                        <?php $i++;
                        } ?>
                    </tbody>
                </table>
                <div>
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>