<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Aktifitas</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Aktifitas
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; <?php echo ($edit) ? "Ubah Data" : "Tambah Data"; ?></div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('activity_name') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Nama Aktifitas</label>
				<div class="col-md-9"><input class="form-control " name="activity_name" value="<?php echo set_value('activity_name', $activity->activity_name); ?>" placeholder="Nama Aktifitas"  required  maxlength=100>
				<?php echo form_error('activity_name'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('activity_description') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Deskripsi</label>
				<div class="col-md-9">
					<textarea class="form-control" name="activity_description" ><?php echo set_value('activity_description', $activity->activity_description); ?></textarea>
					<!--<input class="form-control " name="activity_description" value="<?php echo set_value('activity_description', $activity->activity_description); ?>" placeholder="Deskripsi Aktifitas"   maxlength=255>-->
				<?php echo form_error('activity_description'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('activity_start_date') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Tanggal Mulai</label>
				<div class="col-md-3"><div class="input-group date date-picker" data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
				<input type="text" name="activity_start_date" class="form-control" readonly value="<?php echo set_value('activity_start_date', $activity->activity_start_date); ?>" placeholder="Tanggal Mulai Aktifitas">
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
				<?php echo form_error('activity_start_date'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('activity_end_date') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Tanggal Selesai</label>
				<div class="col-md-3"><div class="input-group date date-picker" data-date="2015-01-01" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
				<input type="text" name="activity_end_date" class="form-control" readonly value="<?php echo set_value('activity_end_date', $activity->activity_end_date); ?>" placeholder="Tanggal Selesai Aktifitas">
				<span class="input-group-btn">
					<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
				<?php echo form_error('activity_end_date'); ?>
				</div>
			</div>
			<!--<div class="form-group <?php echo (form_error('activity_start_time') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Waktu Mulai</label>
				<div class="col-md-9"><input class="form-control timepicker-24" name="activity_start_time" value="<?php echo set_value('activity_start_time', $activity->activity_start_time); ?>" placeholder="Waktu Mulai Aktifitas"  required >
				<?php echo form_error('activity_start_time'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('activity_end_time') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Waktu Selesai</label>
				<div class="col-md-9"><input class="form-control timepicker-24" name="activity_end_time" value="<?php echo set_value('activity_end_time', $activity->activity_end_time); ?>" placeholder="Waktu Selesai Aktifitas"  required >
				<?php echo form_error('activity_end_time'); ?>
				</div>
			</div>--> 
			<?php //print_r($activity->act_status);die(); ?>
			<div class="form-group <?php echo (form_error('act_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-9">
					<label>
						<input type="radio" name="act_status" value="1" <?php echo set_value('act_status', ($activity->act_status == '1') ? "checked" : ""); ?>> Aktif
					</label>
					<label>
						<input type="radio" name="act_status" value="0" <?php echo set_value('act_status', ($activity->act_status === '0') ? "checked" : ""); ?>> Tidak Aktif
					</label>
				<?php echo form_error('act_status'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>