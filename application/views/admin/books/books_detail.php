<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Buku</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Buku
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data Buku</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Kategori Buku:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $books->bookcategory_name; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Judul Buku:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $books->book_title; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Deskripsi:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $books->book_desc; ?></p>
					</div>
				</div>
			</div>
			<?php if(!empty($books->book_file)){?>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">File Buku:</label>
					<div class="col-md-10">
						<p class="form-control-static">
							<div class="inner-content-wrapper" style="height:800px;">
								<object data="<?php echo base_url().$books->book_file; ?>" type="application/pdf" width="100%" height="100%">
								  <p>Alternative text - include a link <a href="<?php echo base_url().$books->book_file; ?>">to the PDF!</a></p>
								</object>
							</div>
						</p>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if(!empty($books->book_cover)){?>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Cover Buku:</label>
					<div class="col-md-10">
						<p class="form-control-static"><img src="<?php echo base_url().$books->book_cover; ?>" width=200px></p>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Pengarang:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $books->book_author; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Penerbit:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo $books->book_publisher; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label col-md-2">Status:</label>
					<div class="col-md-10">
						<p class="form-control-static"><?php echo ($books->book_status == 1)?"Aktif":"Tidak Aktif"; ?></p>
					</div>
				</div>
			</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $books->book_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>