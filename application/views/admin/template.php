<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="MobileOptimized" content="320">
        <title>Direktorat Pembinaan SMA - Content Management System</title>

        <!-- Plugins -->
        <?php
        foreach ($plugins as $plugin) {
            echo "<link href=\"" . base_url() . $plugin . "\" rel=\"stylesheet\" type=\"text/css\"/>";
        }
        ?>

        <!-- Themes -->
        <?php
        foreach ($theme as $tm) {
            echo "<link href=\"" . base_url() . $tm . "\" rel=\"stylesheet\" type=\"text/css\"/>";
        }
        ?>
        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/favicon.png" type="image/x-icon">
        <!-- Basic JS -->
        <?php
        foreach ($basic_js as $js) {
            echo "<script src=\"" . base_url() . $js . "\" type=\"text/javascript\" ></script>";
        }
        ?>
        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
        </script>
    </head>
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <div class="loading-gif">
            <!-- <div class="row">
                <div class="col-md-offset-4 col-md-4 col-md-offset-4">
                    <div class="progress">
                          <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            <span class="percent"></span>
                          </div>
                    </div>        
                </div>
            </div> -->
        </div>
        <!-- BEGIN HEADER -->   
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->  
                <a class="navbar-brand" href="<?php echo base_url() . "admin/beranda"; ?>">
                    <img src="<?php echo base_url() ?>assets/img/logo.png" alt="logo" class="img-responsive" />
                </a>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
                <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <img src="<?php echo base_url() ?>assets/img/menu-toggler.png" alt="" />
                </a> 
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img class="user-icon" alt="" src="<?php echo ((!empty($login_user->user_photo)) ? server_url() . $login_user->user_photo : base_url() . "assets/img/avatar.png"); ?>"/>
                            <span class="username"><?php echo $login_user->user_fullname; ?></span>
                            &nbsp;<i class="fa fa-angle-down fa-fw"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url(); ?>admin/login/logout"><i class="fa fa-key fa-fw"></i>&nbsp; Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->        
                <ul class="page-sidebar-menu">
                    <li>
                        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        <div class="sidebar-toggler hidden-phone"></div>
                        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    </li>
                    <li>
                        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                        <div class="sidebar-search">
                            <div class="form-container">
                                <div class="input-box">
                                    <a href="javascript:;" class="remove"></a>
                                    <input type="text" placeholder="Pencarian Menu" class="menu_search" />
                                    <a href="javascript:;" class="submit menu_submit"></a>
                                </div>
                            </div>
                        </div>
                        <!-- END RESPONSIVE QUICK SEARCH FORM -->
                    </li>
                    <?php
                    $current = $this->uri->segment(2);
                    ?>

                    <?php
                    foreach ($content_menu as $menu) {
                        $current_parent = ($menu->menu_link == $current) ? "class=\"active\"" : "";
						$child = $menu->submenu;
                        ?>
                        <li <?php echo $current_parent; ?>>
                            <a href="<?php echo base_url() . "admin/" . $menu->menu_link ?>" >
                                <?php
                                switch ($menu->menu_name) {
                                    case 'Informasi Website':
                                        echo '<i class="fa fa-info-circle fa-fw"></i>';
                                        break;
                                    case 'Konten':
                                        echo '<i class="fa fa-book fa-fw"></i>';
                                        break;
                                    case 'Tradisi':
                                        echo '<i class="fa fa-book fa-fw"></i>';
                                        break;
                                    case 'Pesan':
                                        echo '<i class="fa fa-envelope fa-fw"></i>';
                                        break;
                                    case 'Pengikut':
                                        echo '<i class="fa fa-group fa-fw"></i>';
                                        break;
                                    case 'Data Master':
                                        echo '<i class="fa fa-table fa-fw"></i>';
                                        break;
                                    case 'Peta':
                                        echo '<i class="fa fa-map-marker fa-fw"></i>';
                                        break;
                                    case 'Pengaturan':
                                        echo '<i class="fa fa-cogs fa-fw"></i>';
                                        break;
                                    case 'Pengaturan':
                                        echo '<i class="fa fa-cogs fa-fw"></i>';
                                        break;
                                    case 'Audit Trail':
                                        echo '<i class="fa fa-tasks fa-fw"></i>';
									case 'Forum':
										echo '<i class="fa fa-comments"></i>';
                                }
                                ?>
                                <span class="title">
                                    <?php echo strtolower($menu->menu_name); ?>
                                    <?php echo ($menu->menu_name == 'Pesan')?"<small class='badge pull-right bg-green'>$message</small>":""; ?>
                                </span>
                                <?php
                                if (!empty($child)) {
                                    ?>
                                    <span class="arrow"></span>
                                <?php }
                                ?>
                                <span class="selected"></span> 
                            </a>
                            <?php if (!empty($child)) {
                                ?>
                                <ul class="sub-menu">
                                    <?php
                                    foreach ($child as $submenu) {
                                        $current_submenu = ($submenu->menu_link == $current) ? "class=\"active\"" : "";
                                        $activePage = $submenu;
                                        ?>				
                                        <li <?php echo $current_submenu; ?>>
                                            <a href="<?php echo base_url() . "admin/" . $submenu->menu_link ?>" >
                                                <span class='title menu-content-load'><?php echo strtolower($submenu->menu_name); ?></span>
                                                <?php if (count($submenu->subsubmenu) != 0) { ?>
                                                    <span class="arrow"></span>
                                                <?php } ?>
                                            </a>
                                            <?php if (!empty($submenu->subsubmenu)) { ?>
                                                <ul class="sub-menu">
                                                    <?php
                                                    foreach ($submenu->subsubmenu as $subsubmenu) {
                                                        $current_subsubmenu = ($subsubmenu->menu_link == $current) ? "class=\"active\"" : "";
                                                        ?>
                                                        <li <?php echo $current_subsubmenu; ?>>
                                                            <a href="<?php echo base_url() . "admin/" . $subsubmenu->menu_link; ?>">
                                                                <span class='title menu-content-load'><?php echo strtolower($subsubmenu->menu_name); ?></span>
                                                            </a>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>	
                                                <?php
                                            }
                                            ?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->  
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->   
                <?php echo $_content; ?>
                <!-- END PAGE CONTENT-->    
            </div>
            <!-- END PAGE -->  
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="footer">
            <div class="footer-inner">

            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <div class="modal fade" id="addmodal" tabindex="-1" aria-hidden="true" data-replace="true">
            <div class="modal-dialog">
                <div class="modal-content ">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addasaldaerah" tabindex="-1" aria-hidden="true" data-replace="true">
            <div class="modal-dialog">
                <div class="modal-content ">
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?php echo base_url() ?>assets/js/respond.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/excanvas.min.js"></script> 
        <![endif]-->   
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <?php
        foreach ($javascript as $js) {
            echo "<script src=\"" . base_url() . $js . "\" type=\"text/javascript\" ></script>";
        }
        ?>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <?php
        foreach ($page_level_plugin as $plg) {
            echo "<script src=\"" . base_url() . $plg . "\" type=\"text/javascript\" ></script>";
        }
        ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <?php
        foreach ($page_level_script as $pls) {
            echo "<script src=\"" . base_url() . $pls . "\" type=\"text/javascript\" ></script>";
        }
        ?>
        <!-- END PAGE LEVEL SCRIPTS -->
        <?php echo $inline_js; ?>
        <!-- END JAVASCRIPTS -->  
    </body>
    <!-- END BODY -->
</html>
<!-- Modal -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Data?</h4>
            </div>
            <div class="modal-body">
                Anda akan menghapus data, Lanjutkan?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-danger danger">Ya</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="del_All" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Data?</h4>
            </div>
            <div class="modal-body">
                Apakah anda akan menghapus data yang ditandai?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                <button type="button" class="btn btn-danger danger2" onclick="go_delete()">Ya</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="nullalert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-basic">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Maaf.</h3>
            </div>
            <div class="modal-body">
                Silahkan Pilih Data. Terima Kasih.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn red" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-small">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Berhasil.</h3>
            </div>
            <div class="modal-body">
                Data berhasil disimpan.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn red" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>