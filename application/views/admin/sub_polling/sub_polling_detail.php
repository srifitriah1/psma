<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Sub Polling</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Sub Polling
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Lihat Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Data Sub Polling</div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="row">
                            
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Id:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_id; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Poll Id:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->poll_id; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Title:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_title; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Status:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_status; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Choice1:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_choice1; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Choice2:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_choice2; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Choice3:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_choice3; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Choice4:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_choice4; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Choice5:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_choice5; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Choice6:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_choice6; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Counter1:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_counter1; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Counter2:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_counter2; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Counter3:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_counter3; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Counter4:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_counter4; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Counter5:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_counter5; ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="control-label col-md-5">Sub Counter6:</label>
					<div class="col-md-7">
						<p class="form-control-static"><?php echo $sub_polling->sub_counter6; ?></p>
					</div>
				</div>
			</div>
							<div class="col-md-12">
								<div class="form-actions fluid">
									<div class="row">
										<div class="col-md-6">
											<div class="col-md-offset-3 col-md-9">
												<a href="<?php echo $current_context .'edit'  .'/'. $sub_polling->sub_id; ?>" class="btn blue"><i class="fa fa-edit"></i> Ubah</a>
												<a href="<?php echo $current_context; ?>" class="btn red">Kembali</a>                              
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>