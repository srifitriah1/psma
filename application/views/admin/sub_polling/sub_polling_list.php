<!-- BEGIN PAGE HEADER-->   
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Sub Polling</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="<?php echo base_url(); ?>">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>Sub Polling</li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet box light-grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-search fa-fw"></i>&nbsp; Pencarian</div>
            </div>
            <div class="portlet-body">
                <form method="post" action="<?php echo $current_context . 'search'; ?>" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-12">
                            <?php $key = (object) $this->session->userdata('filter_sub_polling'); ?>
						<div class="col-md-6"><div class="form-group">
								<label class="control-label col-md-5">Sub Title</label>
								<div class="col-md-7"><input class="form-control " name="sub_title" value="<?php echo $key->sub_title; ?>" placeholder="Sub Title">
								</div>
							</div>
						</div>
                            <input type="hidden" id="search" name="search" value="true">
                            <div class="clearfix">
                                <button type="submit" class="btn green pull-right">Lakukan Pencarian</button>
                                <button type="button" class="btn default pull-right" onclick="location.href='<?php echo $current_context; ?>'">Kosongkan Pencarian</button>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
<?php
	$message = $this->session->flashdata('message');
	$type_message = $this->session->flashdata('type_message');
	echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
	echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-table fa-fw"></i>&nbsp; Tabel Data (<?php echo $total_rows;?> Data)</div> <!-- SHOW TITLE AND NUMBER OF DATA -->
            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="btn-group">
                        <div class="clearfix">
							<a href="" id="deleteall" data-href="<?php echo $current_context . 'delete_multiple/'; ?>" class="btn red" disabled="true" data-toggle="modal" data-target="#del_All"><i class="fa fa-trash fa-fw"></i>&nbsp; Hapus Data</a>
                            <a id="" class="btn green" href="<?php echo $current_context . 'add/'; ?>">
                                Tambah &nbsp;<i class="fa fa-plus fa-fw"></i>
                            </a>
                            <a class="btn purple" href="#" data-toggle="dropdown">
                                Kolom &nbsp;
                                <i class="fa fa-angle-down fa-fw"></i>
                            </a>
                            <div id="table_data_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                <label><input type="checkbox" checked data-column="0">Checkbox</label>
                                <label><input type="checkbox" checked data-column="1">Sub Id</label>
							<label><input type="checkbox" checked data-column="2">Poll Id</label>
							<label><input type="checkbox" checked data-column="3">Sub Title</label>
							<label><input type="checkbox" checked data-column="4">Sub Status</label>
							<label><input type="checkbox" checked data-column="5">Sub Choice1</label>
							<label><input type="checkbox" checked data-column="6">Sub Choice2</label>
							<label><input type="checkbox" checked data-column="7">Sub Choice3</label>
							<label><input type="checkbox" checked data-column="8">Sub Choice4</label>
							<label><input type="checkbox" checked data-column="9">Sub Choice5</label>
							<label><input type="checkbox" checked data-column="10">Sub Choice6</label>
							<label><input type="checkbox" checked data-column="11">Sub Counter1</label>
							<label><input type="checkbox" checked data-column="12">Sub Counter2</label>
							<label><input type="checkbox" checked data-column="13">Sub Counter3</label>
							<label><input type="checkbox" checked data-column="14">Sub Counter4</label>
							<label><input type="checkbox" checked data-column="15">Sub Counter5</label>
							<label><input type="checkbox" checked data-column="16">Sub Counter6</label>
                            </div>

                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" id="table_data">
                    <thead>
                        <tr>
                            <th class="table-checkbox"><input type="checkbox" class="group-checkable" data-set="#table_data .checkboxes" /></th>
                            <th>Sub Id</th>
						<th>Poll Id</th>
						<th>Sub Title</th>
						<th>Sub Status</th>
						<th>Sub Choice1</th>
						<th>Sub Choice2</th>
						<th>Sub Choice3</th>
						<th>Sub Choice4</th>
						<th>Sub Choice5</th>
						<th>Sub Choice6</th>
						<th>Sub Counter1</th>
						<th>Sub Counter2</th>
						<th>Sub Counter3</th>
						<th>Sub Counter4</th>
						<th>Sub Counter5</th>
						<th>Sub Counter6</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($sub_polling as $row) {
                            ?>
                            <tr class="odd gradeX">
                                <td><input type="checkbox" class="checkboxes"  data-sub_id="<?php echo $row->sub_id; ?>"  /></td><td><?php echo $row->sub_id; ?></td>
							<td><?php echo $row->poll_id; ?></td>
							<td><?php echo $row->sub_title; ?></td>
							<td><?php echo $row->sub_status; ?></td>
							<td><?php echo $row->sub_choice1; ?></td>
							<td><?php echo $row->sub_choice2; ?></td>
							<td><?php echo $row->sub_choice3; ?></td>
							<td><?php echo $row->sub_choice4; ?></td>
							<td><?php echo $row->sub_choice5; ?></td>
							<td><?php echo $row->sub_choice6; ?></td>
							<td><?php echo $row->sub_counter1; ?></td>
							<td><?php echo $row->sub_counter2; ?></td>
							<td><?php echo $row->sub_counter3; ?></td>
							<td><?php echo $row->sub_counter4; ?></td>
							<td><?php echo $row->sub_counter5; ?></td>
							<td><?php echo $row->sub_counter6; ?></td>
							<td>
								<a href="<?php echo $current_context . 'detail'  .'/'. $row->sub_id ?>" class="btn green btn-xs"><i class="fa fa-eye fa-fw"></i>lihat</a>
								<a href="<?php echo $current_context . 'edit'  .'/'. $row->sub_id ?>" class="btn blue btn-xs"><i class="fa fa-edit fa-fw"></i>ubah</a>
								<a href="#" data-href="<?php echo $current_context . 'delete'  .'/'. $row->sub_id ?>" data-toggle="modal" data-target="#deleteModal"  class="btn red btn-xs"><i class="fa fa-trash fa-fw"></i>hapus</a>
							</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div>
                    <?php echo $pagination; ?>
                </div>
            </div>
        </div>
    </div>
</div>