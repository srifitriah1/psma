<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">Sub Polling</h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="fa fa-home fa-fw"></i>&nbsp;<a href="">Beranda </a> 
                &nbsp;<i class="fa fa-angle-right fa-fw"></i> <!-- UNCOMMENT THIS WHEN BREADCRUMB FILLED MORE THEN ONE PAGE -->
            </li>
            <li>
                Sub Polling
                &nbsp;<i class="fa fa-angle-right fa-fw"></i>
            </li>
            <li>
                Tambah Data
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<!-- EVERY CONTENT BEGIN WITH ROW > COL-MD-* > PORTLET BOX > ..., COPY AND PASTE IT FOR EASY STEP -->
<!-- PLEASE FOLLOW THE GOD DAMN STRUCTURE, YOU WILL DESTROY THE DESIGN IF YOU AIN'T AND ASK RUCHI FOR HELP -->
<!-- NOTE: PLASE MAINTAIN THE DEPTH OF EACH HTML TAGS -->
<div class="row">
	<?php
		$message = $this->session->flashdata('message');
		$type_message = $this->session->flashdata('type_message');
		echo (!empty($message) && $type_message=="success") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
		echo (!empty($message) && $type_message=="error") ? '	<div class="col-md-12" id="data-alert-box"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><strong>Error! </strong>'.$message.'</div></div>': '';
	?>
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-plus fa-fw"></i>&nbsp; Tambah Data</div>
            </div>
            <div class="portlet-body form">
                <form method="post" action="" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data" >
                    <div class="form-body">
                        
			<div class="form-group <?php echo (form_error('poll_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Poll Id</label>
				<div class="col-md-9"><select class="form-control select2me" name="poll_id" data-placeholder="Pilih..." >
					<option></option>
					<?php
					foreach($polling_list as $polling){
						if($polling->polling_id == set_value('poll_id',$sub_polling->poll_id)){
							echo "<option value='$polling->polling_id' selected>$polling->polling_id</option>";
						} else {
							echo "<option value='$polling->polling_id'>$polling->polling_id</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('poll_id'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_title') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Title</label>
				<div class="col-md-9"><input class="form-control " name="sub_title" value="<?php echo set_value('sub_title', $sub_polling->sub_title); ?>" placeholder="Sub Title"  required  maxlength=100>
				<?php echo form_error('sub_title'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_status') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Status</label>
				<div class="col-md-9"><input class="form-control mask_number" name="sub_status" value="<?php echo set_value('sub_status', $sub_polling->sub_status); ?>" placeholder="Sub Status"  required  maxlength=3>
				<?php echo form_error('sub_status'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_choice1') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Choice1</label>
				<div class="col-md-9"><input class="form-control " name="sub_choice1" value="<?php echo set_value('sub_choice1', $sub_polling->sub_choice1); ?>" placeholder="Sub Choice1"  required  maxlength=100>
				<?php echo form_error('sub_choice1'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_choice2') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Choice2</label>
				<div class="col-md-9"><input class="form-control " name="sub_choice2" value="<?php echo set_value('sub_choice2', $sub_polling->sub_choice2); ?>" placeholder="Sub Choice2"  required  maxlength=100>
				<?php echo form_error('sub_choice2'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_choice3') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Choice3</label>
				<div class="col-md-9"><input class="form-control " name="sub_choice3" value="<?php echo set_value('sub_choice3', $sub_polling->sub_choice3); ?>" placeholder="Sub Choice3"  maxlength=100>
				<?php echo form_error('sub_choice3'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_choice4') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Choice4</label>
				<div class="col-md-9"><input class="form-control " name="sub_choice4" value="<?php echo set_value('sub_choice4', $sub_polling->sub_choice4); ?>" placeholder="Sub Choice4"  maxlength=100>
				<?php echo form_error('sub_choice4'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_choice5') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Choice5</label>
				<div class="col-md-9"><input class="form-control " name="sub_choice5" value="<?php echo set_value('sub_choice5', $sub_polling->sub_choice5); ?>" placeholder="Sub Choice5"  maxlength=100>
				<?php echo form_error('sub_choice5'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_choice6') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Choice6</label>
				<div class="col-md-9"><input class="form-control " name="sub_choice6" value="<?php echo set_value('sub_choice6', $sub_polling->sub_choice6); ?>" placeholder="Sub Choice6"  maxlength=100>
				<?php echo form_error('sub_choice6'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_counter1') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Counter1</label>
				<div class="col-md-9"><input class="form-control mask_number" name="sub_counter1" value="<?php echo set_value('sub_counter1', $sub_polling->sub_counter1); ?>" placeholder="Sub Counter1"  maxlength=10>
				<?php echo form_error('sub_counter1'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_counter2') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Counter2</label>
				<div class="col-md-9"><input class="form-control mask_number" name="sub_counter2" value="<?php echo set_value('sub_counter2', $sub_polling->sub_counter2); ?>" placeholder="Sub Counter2"  maxlength=10>
				<?php echo form_error('sub_counter2'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_counter3') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Counter3</label>
				<div class="col-md-9"><input class="form-control mask_number" name="sub_counter3" value="<?php echo set_value('sub_counter3', $sub_polling->sub_counter3); ?>" placeholder="Sub Counter3"  maxlength=10>
				<?php echo form_error('sub_counter3'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_counter4') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Counter4</label>
				<div class="col-md-9"><input class="form-control mask_number" name="sub_counter4" value="<?php echo set_value('sub_counter4', $sub_polling->sub_counter4); ?>" placeholder="Sub Counter4"  maxlength=10>
				<?php echo form_error('sub_counter4'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_counter5') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Counter5</label>
				<div class="col-md-9"><input class="form-control mask_number" name="sub_counter5" value="<?php echo set_value('sub_counter5', $sub_polling->sub_counter5); ?>" placeholder="Sub Counter5"  maxlength=10>
				<?php echo form_error('sub_counter5'); ?>
				</div>
			</div>
			<div class="form-group <?php echo (form_error('sub_counter6') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-3">Sub Counter6</label>
				<div class="col-md-9"><input class="form-control mask_number" name="sub_counter6" value="<?php echo set_value('sub_counter6', $sub_polling->sub_counter6); ?>" placeholder="Sub Counter6"  maxlength=10>
				<?php echo form_error('sub_counter6'); ?>
				</div>
			</div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn blue"><i class="fa fa-check"></i> Simpan</button>
                                        <button type="button" class="btn red" onclick="history.go(-1);">Batal</button>                              
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>