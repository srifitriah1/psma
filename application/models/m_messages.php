<?php
class M_messages extends Generic_dao {

    public function table_name() {
        return Tables::$messages;
    }

    public function field_map() {
		return array(
			'message_id' => 'MESSAGE_ID',
			'message_name' => 'MESSAGE_NAME',
			'message_email' => 'MESSAGE_EMAIL',
			'message_title' => 'MESSAGE_TITLE',
			'message_content' => 'MESSAGE_CONTENT',
			'message_date' => 'MESSAGE_DATE',
			'message_status' => 'MESSAGE_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();


    }

}

?>