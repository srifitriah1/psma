<?php
class M_layanan extends Generic_dao {

    public function table_name() {
        return Tables::$layanan;
    }

    public function field_map() {
		return array(
			'layanan_id' => 'LAYANAN_ID',
			'layanan_title' => 'LAYANAN_TITLE',
			'layanan_link' => 'LAYANAN_LINK',
			'layanan_status' => 'LAYANAN_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>