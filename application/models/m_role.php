<?php
class M_role extends Generic_dao {

    public function table_name() {
        return Tables::$role;
    }

    public function field_map() {
		return array(
			'role_id' => 'ROLE_ID',
			'role_name' => 'ROLE_NAME',
			'role_status' => 'ROLE_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>