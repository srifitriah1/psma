<?php
class M_testimonial extends Generic_dao {

    public function table_name() {
        return Tables::$testimonial;
    }

    public function field_map() {
		return array(
			'testimonial_id' => 'TESTI_ID',
			'testimonial_name' => 'TESTI_NAME',
			'testimonial_photo' => 'TESTI_PHOTO',
			'testimonial_content' => 'TESTI_CONTENT',
			'testimonial_date' => 'TESTI_DATE',
			'testimonial_status' => 'TESTI_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>