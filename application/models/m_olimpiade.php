<?php
class M_olimpiade extends Generic_dao {

    public function table_name() {
        return Tables::$olimpiade;
    }

    public function field_map() {
		return array(
			'olimpiade_id' => 'OLMPAD_ID',
			'user_id' => 'USER_ID',
			'olimpiade_title' => 'OLMPAD_TITLE',
			'olimpiade_summary' => 'OLMPAD_SUMMARY',
			'olimpiade_content' => 'OLMPAD_CONTENT',
			'olimpiade_quotes' => 'OLMPAD_QUOTES',
			'olimpiade_photo' => 'OLMPAD_PHOTO',
			'olimpiade_posted_on' => 'OLMPAD_POSTED_ON',
			'olimpiade_status' => 'OLMPAD_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'model_name' => "m_users",
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID'
            )
        );
    }

    public function get_title_list($olimpiade_title = null){
        $this->ci->db->select('olimpiade_title');
        $this->ci->db->where('olimpiade_status',1);
        $this->ci->db->like('olimpiade_title',$olimpiade_title);
        $result = $this->ci->db->get($this->table_name());
        return $result->result();
    }

}

?>