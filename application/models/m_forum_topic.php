<?php
class M_forum_topic extends Generic_dao {

    public function table_name() {
        return Tables::$forum_topic;
    }

    public function field_map() {
		return array(
			'topic_id' => 'TOPIC_ID',
			'topic_title' => 'TOPIC_TITLE',
			'topic_created' => 'TOPIC_CREATED',
			'topic_status' => 'TOPIC_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>