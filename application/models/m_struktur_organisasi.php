<?php
class M_struktur_organisasi extends Generic_dao {

    public function table_name() {
        return Tables::$struktur_organisasi;
    }

    public function field_map() {
		return array(
			'struktur_id' => 'STORG_ID',
			'user_id' => 'USER_ID',
			'struktur_name' => 'STORG_NAME',
			'struktur_position' => 'STORG_POSITION',
			'struktur_desc' => 'STORG_DESC',
			'struktur_photo' => 'STORG_PHOTO',
			'struktur_phone' => 'STORG_PHONE',
			'struktur_email' => 'STORG_EMAIL',
			'struktur_facebook' => 'STORG_FACEBOOK',
			'struktur_twitter' => 'STORG_TWITTER',
			'struktur_status' => 'STORG_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID',
                'field' => 'USER_FULLNAME as user_fullname, USER_USERNAME as user_username'
            )
        );
    }

}

?>