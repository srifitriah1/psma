<?php
class M_book_category extends Generic_dao {

    public function table_name() {
        return Tables::$book_category;
    }

    public function field_map() {
		return array(
			'bookcategory_id' => 'BCAT_ID',
			'bookcategory_name' => 'BCAT_NAME',
			'bookcategory_status' => 'BCAT_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>