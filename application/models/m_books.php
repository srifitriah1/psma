<?php
class M_books extends Generic_dao {

    public function table_name() {
        return Tables::$books;
    }

    public function field_map() {
		return array(
			'book_id' => 'BOOK_ID',
			'bookcategory_id' => 'BCAT_ID',
			'book_title' => 'BOOK_TITLE',
			'book_desc' => 'BOOK_DESC',
			'book_file' => 'BOOK_FILE',
            'book_cover' => 'BOOK_COVER',
			'book_status' => 'BOOK_STATUS',
			'book_author' => 'BOOK_AUTHOR',
			'book_publisher' => 'BOOK_PUBLISHER'
		);
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$book_category,
                'condition' => Tables::$book_category . '.BCAT_ID = ' . $this->table_name() . '.BCAT_ID',
                'field' => 'BCAT_NAME as bookcategory_name, BCAT_STATUS as status'
            )
        );
    }

}

?>