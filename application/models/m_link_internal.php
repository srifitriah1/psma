<?php
class M_link_internal extends Generic_dao {

    public function table_name() {
        return Tables::$link_internal;
    }

    public function field_map() {
		return array(
			'link_id' => 'LINK_ID',
			'link_title' => 'LINK_TITLE',
			'link_link' => 'LINK_LINK',
			'link_status' => 'LINK_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>