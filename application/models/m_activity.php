<?php
class M_activity extends Generic_dao {

    public function table_name() {
        return Tables::$activity;
    }

    public function field_map() {
		return array(
			'activity_id' => 'ACT_ID',
			'activity_name' => 'ACT_NAME',
			'activity_description' => 'ACT_DESCRIPTION',
			'activity_start_date' => 'ACT_START_DATE',
			'activity_end_date' => 'ACT_END_DATE',
			'activity_start_time' => 'ACT_START_TIME',
			'activity_end_time' => 'ACT_END_TIME',
			'act_status' => 'ACT_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>