<?php
class M_activity_log extends Generic_dao {

    public function table_name() {
        return Tables::$activity_log;
    }

    public function field_map() {
		return array(
			'log_id' => 'LOG_ID',
			'news_id' => 'NEWS_ID',
			'storg_id' => 'STORG_ID',
			'user_id' => 'USER_ID',
			'static_id' => 'STATIC_ID',
			'log_desc' => 'LOG_DESC',
			'log_type' => 'LOG_TYPE',
			'log_datetime' => 'LOG_DATETIME',
			'log_before' => 'LOG_BEFORE',
			'log_after' => 'LOG_AFTER'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>