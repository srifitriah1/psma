<?php
class M_photo_gallery extends Generic_dao {

    public function table_name() {
        return Tables::$photo_gallery;
    }

    public function field_map() {
		return array(
			'gallery_id' => 'GALLERY_ID',
			'gallery_photo' => 'GALLERY_PHOTO',
			'gallery_caption' => 'GALLERY_CAPTION',
			'gallery_status' => 'GALLERY_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>