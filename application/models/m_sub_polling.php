<?php
class M_sub_polling extends Generic_dao {

    public function table_name() {
        return Tables::$sub_polling;
    }

    public function field_map() {
		return array(
			'sub_id' => 'SUB_ID',
			'poll_id' => 'POLL_ID',
			'sub_title' => 'SUB_TITLE',
			'sub_status' => 'SUB_STATUS',
			'sub_choice1' => 'SUB_CHOICE1',
			'sub_choice2' => 'SUB_CHOICE2',
			'sub_choice3' => 'SUB_CHOICE3',
			'sub_choice4' => 'SUB_CHOICE4',
			'sub_choice5' => 'SUB_CHOICE5',
			'sub_choice6' => 'SUB_CHOICE6',
			'sub_counter1' => 'SUB_COUNTER1',
			'sub_counter2' => 'SUB_COUNTER2',
			'sub_counter3' => 'SUB_COUNTER3',
			'sub_counter4' => 'SUB_COUNTER4',
			'sub_counter5' => 'SUB_COUNTER5',
			'sub_counter6' => 'SUB_COUNTER6'
		);
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$polling,
                'condition' => Tables::$polling . '.POLL_ID = ' . $this->table_name() . '.POLL_ID',
                'field' => 'POLL_TITLE as polling_title'
            )
        );
    }

}

?>