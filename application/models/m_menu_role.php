<?php
class M_menu_role extends Generic_dao {

    public function table_name() {
        return Tables::$menu_role;
    }

    public function field_map() {
		return array(
			'role_id' => 'ROLE_ID',
			'menu_id' => 'MENU_ID'
		);
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$menu,
                'condition' => Tables::$menu . '.menu_id = ' . $this->table_name() . '.menu_id',
                'field' => 'MENU_NAME as menu_name'
            )
        );
    }

}

?>