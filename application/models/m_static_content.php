<?php
class M_static_content extends Generic_dao {

    public function table_name() {
        return Tables::$static_content;
    }

    public function field_map() {
		return array(
			'static_id' => 'STATIC_ID',
			'user_id' => 'USER_ID',
			'static_title' => 'STATIC_TITLE',
			'static_content' => 'STATIC_CONTENT',
			'static_status' => 'STATIC_STATUS',
			'static_iscontactinfo' => 'STATIC_ISCONTACTINFO'
		);
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID',
                'field' => 'USER_FULLNAME as user_fullname, USER_USERNAME as user_username'
            )
        );
    }

}

?>