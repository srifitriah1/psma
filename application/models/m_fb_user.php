<?php
class M_fb_user extends Generic_dao {

    public function table_name() {
        return Tables::$fb_user;
    }

    public function field_map() {
		return array(
			'fb_id' => 'FB_ID',
			'fb_name' => 'FB_NAME',
			'fb_isbanned' => 'FB_ISBANNED',
			'fb_banreason' => 'FB_BANREASON'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>