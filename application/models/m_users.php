<?php
class M_users extends Generic_dao {

    public function table_name() {
        return Tables::$users;
    }

    public function field_map() {
		return array(
			'user_id' => 'USER_ID',
			'role_id' => 'ROLE_ID',
			'user_fullname' => 'USER_FULLNAME',
			'user_username' => 'USER_USERNAME',
			'user_email' => 'USER_EMAIL',
			'user_password' => 'USER_PASSWORD',
			'user_photo' => 'USER_PHOTO',
			'user_registered_on' => 'USER_REGISTERED_ON',
			'user_status' => 'USER_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function UpdateData($table,$data,$where){
        return $this->ci->db->update($table,$data,$where);
    }

	public function check_login($username, $password) {
        $password = md5($password);
        $query = $this->ci->db->query("select * from users where user_username='$username' and user_password='$password'");
        return ($query->num_rows() == 1);
    }
	
	public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$role,
                'condition' => Tables::$role . '.ROLE_ID = ' . $this->table_name() . '.ROLE_ID',
                'field' => 'ROLE_NAME as role_name'
            )
        );
    }
}

?>