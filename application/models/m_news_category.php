<?php
class M_news_category extends Generic_dao {

    public function table_name() {
        return Tables::$news_category;
    }

    public function field_map() {
		return array(
			'category_id' => 'CAT_ID',
			'category_name' => 'CAT_NAME',
			'category_status' => 'CAT_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>