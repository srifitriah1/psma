<?php
class M_website_info extends Generic_dao {

    public function table_name() {
        return Tables::$website_info;
    }

    public function field_map() {
		return array(
			'id' => 'ID',
			'title' => 'TITLE',
			'meta_title' => 'META_TITLE',
			'meta_description' => 'META_DESCRIPTION',
			'address_title' => 'ADDRESS_TITLE',
			'address' => 'ADDRESS',
			'address_latitude' => 'ADDRESS_LATITUDE',
			'address_longitude' => 'ADDRESS_LONGITUDE',
			'phone' => 'PHONE',
			'fax' => 'FAX',
			'email' => 'EMAIL',
			'live_tv' => 'LIVE_TV',
			'twitter' => 'TWITTER',
			'facebook' => 'FACEBOOK',
			'youtube' => 'YOUTUBE'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>