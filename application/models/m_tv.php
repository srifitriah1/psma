<?php
class M_tv extends Generic_dao {

    public function table_name() {
        return Tables::$tv;
    }

    public function field_map() {
		return array(
			'tv_id' => 'tv_id',
			'tv_link' => 'tv_link',
			'tv_status' => 'tv_status',
			'tv_datetime' => 'tv_datetime',
			'tv_title' => 'tv_title'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>