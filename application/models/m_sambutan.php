<?php
class M_sambutan extends Generic_dao {

    public function table_name() {
        return Tables::$sambutan;
    }

    public function field_map() {
		return array(
			'sambutan_id' => 'SAMBUTAN_ID',
			'pemberi_sambutan' => 'PEMBERI_SAMBUTAN',
			'isi_sambutan' => 'ISI_SAMBUTAN',
			'sambutan_status' => 'SAMBUTAN_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>