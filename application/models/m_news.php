<?php
class M_news extends Generic_dao {

    public function table_name() {
        return Tables::$news;
    }

    public function field_map() {
		return array(
			'news_id' => 'NEWS_ID',
			'user_id' => 'USER_ID',
			'cat_id' => 'CAT_ID',
			'news_title' => 'NEWS_TITLE',
			'news_summary' => 'NEWS_SUMMARY',
			'news_content' => 'NEWS_CONTENT',
			'news_quotes' => 'NEWS_QUOTES',
			'news_photo' => 'NEWS_PHOTO',
			'news_posted_on' => 'NEWS_POSTED_ON',
			'news_status' => 'NEWS_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'model_name' => "m_news_category",
                'table_name' => Tables::$news_category,
                'condition' => Tables::$news_category . '.CAT_ID = ' . $this->table_name() . '.CAT_ID'
            ),
            array(
                'model_name' => "m_users",
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID'
            )
        );
    }

    public function get_title_list($news_title = null){
        $this->ci->db->select('news_title');
        $this->ci->db->where('news_status',1);
        $this->ci->db->like('news_title',$news_title);
        $result = $this->ci->db->get($this->table_name());
        return $result->result();
    }

}

?>