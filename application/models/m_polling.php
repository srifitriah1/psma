<?php
class M_polling extends Generic_dao {

    public function table_name() {
        return Tables::$polling;
    }

    public function field_map() {
		return array(
			'polling_id' => 'POLL_ID',
			'polling_author' => 'USER_ID',
			'polling_title' => 'POLL_TITLE',
			'polling_image' => 'POLL_IMAGE',
			'polling_description' => 'POLL_DESCRIPTION',
			'polling_posted_on' => 'POLL_POSTED_ON',
			'polling_status' => 'POLL_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }
	
    public function joined_table() {
        return array(
            array(
                'model_name' => "m_users",
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID'
            )
        );
    }

}

?>