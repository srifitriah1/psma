<?php
class M_pembelajaran extends Generic_dao {

    public function table_name() {
        return Tables::$pembelajaran;
    }

    public function field_map() {
		return array(
			'pembelajaran_id' => 'PMBLJRN_ID',
			'user_id' => 'USER_ID',
			'pembelajaran_title' => 'PMBLJRN_TITLE',
			'pembelajaran_summary' => 'PMBLJRN_SUMMARY',
			'pembelajaran_content' => 'PMBLJRN_CONTENT',
			'pembelajaran_quotes' => 'PMBLJRN_QUOTES',
			'pembelajaran_photo' => 'PMBLJRN_PHOTO',
			'pembelajaran_posted_on' => 'PMBLJRN_POSTED_ON',
			'pembelajaran_status' => 'PMBLJRN_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'model_name' => "m_users",
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID'
            )
        );
    }

    public function get_title_list($pembelajaran_title = null){
        $this->ci->db->select('pembelajaran_title');
        $this->ci->db->where('pembelajaran_status',1);
        $this->ci->db->like('pembelajaran_title',$pembelajaran_title);
        $result = $this->ci->db->get($this->table_name());
        return $result->result();
    }

}

?>