<?php
class M_sarana extends Generic_dao {

    public function table_name() {
        return Tables::$sarana;
    }

    public function field_map() {
		return array(
			'sarana_id' => 'SARANA_ID',
			'user_id' => 'USER_ID',
			'sarana_title' => 'SARANA_TITLE',
			'sarana_summary' => 'SARANA_SUMMARY',
			'sarana_content' => 'SARANA_CONTENT',
			'sarana_quotes' => 'SARANA_QUOTES',
			'sarana_photo' => 'SARANA_PHOTO',
			'sarana_posted_on' => 'SARANA_POSTED_ON',
			'sarana_status' => 'SARANA_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'model_name' => "m_users",
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID'
            )
        );
    }

    public function get_title_list($sarana_title = null){
        $this->ci->db->select('sarana_title');
        $this->ci->db->where('sarana_status',1);
        $this->ci->db->like('sarana_title',$sarana_title);
        $result = $this->ci->db->get($this->table_name());
        return $result->result();
    }

}

?>