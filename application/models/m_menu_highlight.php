<?php
class M_menu_highlight extends Generic_dao {

    public function table_name() {
        return Tables::$menu_highlight;
    }

    public function field_map() {
		return array(
			'highlight_id' => 'HI_ID',
			'highlight_title' => 'HI_TITLE',
			'highlight_image' => 'HI_IMAGE',
			'highlight_link' => 'HI_LINK',
			'highlight_status' => 'HI_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>