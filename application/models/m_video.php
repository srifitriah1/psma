<?php
class M_video extends Generic_dao {

    public function table_name() {
        return Tables::$video;
    }

    public function field_map() {
		return array(
			'video_id' => 'video_id',
			'video_link' => 'video_link',
			'video_status' => 'video_status',
			'video_datetime' => 'video_datetime',
			'video_title' => 'video_title'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>