<?php
class M_file_download extends Generic_dao {

    public function table_name() {
        return Tables::$file_download;
    }

    public function field_map() {
		return array(
			'file_id' => 'FILE_ID',
			'file_name' => 'FILE_NAME',
			'file_link' => 'FILE_LINK',
			'file_status' => 'FILE_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>