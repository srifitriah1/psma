<?php
class M_news_comment extends Generic_dao {

    public function table_name() {
        return Tables::$news_comment;
    }

    public function field_map() {
		return array(
			'comment_id' => 'COMMENT_ID',
			'parent_comment_id' => 'NEW_COMMENT_ID',
			'news_id' => 'NEWS_ID',
			'comment_name' => 'COMMENT_NAME',
			'comment_email' => 'COMMENT_EMAIL',
			'comment_website' => 'COMMENT_WEBSITE',
			'comment_content' => 'COMMENT_CONTENT',
			'comment_avatar' => 'COMMENT_AVATAR',
			'comment_status' => 'COMMENT_STATUS',
			'comment_datetime' => 'COMMENT_DATETIME'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>