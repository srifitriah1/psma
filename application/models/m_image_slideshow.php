<?php
class M_image_slideshow extends Generic_dao {

    public function table_name() {
        return Tables::$image_slideshow;
    }

    public function field_map() {
		return array(
			'slide_id' => 'SLIDE_ID',
			'slide_image' => 'SLIDE_IMAGE',
			'slide_title' => 'SLIDE_TITLE',
			'slide_status' => 'SLIDE_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>