<?php
class M_menu extends Generic_dao {

    public function table_name() {
        return Tables::$menu;
    }

    public function field_map() {
		return array(
			'menu_id' => 'MENU_ID',
			'parent_menu_id' => 'MEN_MENU_ID',
			'menu_name' => 'MENU_NAME',
			'menu_link' => 'MENU_LINK',
			'menu_status' => 'MENU_STATUS',
			'menu_isparent' => 'MENU_ISPARENT',
			'menu_order' => 'MENU_ORDER'
		);
    }

    public function __construct() {
        parent::__construct();
    }
	
	public function get_active_menu($role_id, $parent = null) {
        if ($parent == null) {
            $sql = "select a.menu_id, a.menu_name, a.menu_link from menu a inner join menu_role b
				ON a.menu_id=b.menu_id 
				where a.menu_status = 1 and b.role_id='$role_id' and a.menu_isparent='1' order by menu_order asc";
        } else {
            $sql = "SELECT a.menu_id, a.menu_name, a.menu_link from menu a "
                    . "inner join menu_role b ON a.menu_id=b.menu_id "
                    . "where a.men_menu_id = $parent and a.menu_status = 1 and b.role_id ='$role_id' order by menu_order asc";
        }
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

	public function get_whole_menu($parent = null) {
        if ($parent == null) {
            $sql = "select menu_id, menu_name, menu_link from menu 
				where menu_status = 1 and menu_isparent= '1'";
        } else {
            $sql = "SELECT menu_id, menu_name, menu_link from menu where men_menu_id = $parent and menu_status = 1";
        }
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

	public function get_menu($role_id) {
        $sql = "select menu_id from menu_role where role_id='$role_id'";
        $query = $this->ci->db->query($sql);
        return $query->result();
    }
	
    public function joined_table() {
        return array(
			array(
                'table_name' => Tables::$menu ." as parent ",
                'condition' => 'parent.MENU_ID = '.$this->table_name().'.MEN_MENU_ID',
                'field' => 'parent.MENU_NAME as parent_menu_name',
                'direction' => 'left'
            )
        );
    }
}

?>