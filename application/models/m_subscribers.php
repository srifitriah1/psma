<?php
class M_subscribers extends Generic_dao {

    public function table_name() {
        return Tables::$subscribers;
    }

    public function field_map() {
		return array(
			'subscriber_id' => 'SUBSCRIBER_ID',
			'subscriber_email' => 'SUBSCRIBER_EMAIL',
			'subscriber_date' => 'SUBSCRIBER_DATE',
			'subscriber_status' => 'SUBSCRIBER_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>