<?php
class M_bos extends Generic_dao {

    public function table_name() {
        return Tables::$bos;
    }

    public function field_map() {
		return array(
			'bos_id' => 'BOS_ID',
			'user_id' => 'USER_ID',
			'bos_title' => 'BOS_TITLE',
			'bos_summary' => 'BOS_SUMMARY',
			'bos_content' => 'BOS_CONTENT',
			'bos_quotes' => 'BOS_QUOTES',
			'bos_photo' => 'BOS_PHOTO',
			'bos_posted_on' => 'BOS_POSTED_ON',
			'bos_status' => 'BOS_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'model_name' => "m_users",
                'table_name' => Tables::$users,
                'condition' => Tables::$users . '.USER_ID = ' . $this->table_name() . '.USER_ID'
            )
        );
    }

    public function get_title_list($bos_title = null){
        $this->ci->db->select('bos_title');
        $this->ci->db->where('bos_status',1);
        $this->ci->db->like('bos_title',$bos_title);
        $result = $this->ci->db->get($this->table_name());
        return $result->result();
    }

}

?>