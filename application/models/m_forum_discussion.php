<?php
class M_forum_discussion extends Generic_dao {

    public function table_name() {
        return Tables::$forum_discussion;
    }

    public function field_map() {
		return array(
			'discuss_id' => 'DISCUSS_ID',
			'topic_id' => 'TOPIC_ID',
			'fb_id' => 'FB_ID',
			'discuss_comment' => 'DISCUSS_COMMENT',
			'discuss_datetime' => 'DISCUSS_DATETIME',
			'discuss_status' => 'DISCUSS_STATUS'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'model_name' => "m_fb_user",
                'table_name' => Tables::$fb_user,
                'condition' => Tables::$fb_user . '.FB_ID = ' . $this->table_name() . '.FB_ID'
            ),
            array(
                'model_name' => "m_forum_topic",
                'table_name' => Tables::$forum_topic,
                'condition' => Tables::$forum_topic . '.TOPIC_ID = ' . $this->table_name() . '.TOPIC_ID'
            )
        );
    }

}

?>