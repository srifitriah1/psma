<?php

class Template_admin {

    public $current_user;
    private $obj_id;
	private $message;

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->library('session');
        $this->ci->load->model(array('m_users','m_menu','m_messages'));
        $this->preload();
    }

    function preload(){
        $this->obj_id = array('user_username' => $this->ci->session->userdata('username'));
        $this->current_user = $this->ci->m_users->by_id($this->obj_id);
		$key = array('message_status'=>0);
		$this->message = $this->ci->m_messages->count_all($key);
    }

    function display($template, $data = null, $type = "list") {
        if (!empty($this->current_user)) {
            $current_menu = $this->ci->uri->segment(2);
            $basic_js = array(
                'assets/js/jquery-1.10.2.min.js',
                'assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js');
            switch ($type) {
                case "detail" :
                case "insert" :
                    $plugin = array(
                        'assets/plugins/fontawesome/css/font-awesome.min.css',
                        'assets/plugins/bootstrap/css/bootstrap.css',
                        'assets/plugins/uniform/css/uniform.default.css',
                        'assets/plugins/select2/select2_metro.css',
                        'assets/plugins/data-tables/DT_bootstrap.css',
                        'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css',
                        'assets/plugins/bootstrap-datepicker/css/datepicker.css',
                        'assets/plugins/bootstrap-timepicker/compiled/timepicker.css',
                        'assets/plugins/video-js/video-js.css'
                    );
                    $theme = array(
                        'assets/css/style-metronic.css',
                        'assets/css/style.css',
                        'assets/css/style-responsive.css',
                        'assets/css/plugins.css',
                        'assets/css/light.css',
                        'assets/css/custom.css'
                    );
                    $javascript = array(
                        'assets/plugins/bootstrap/js/bootstrap.min.js',
                        'assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js',
                        'assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
                        'assets/js/jquery.blockui.min.js',
                        'assets/js/jquery.cookie.min.js',
                        'assets/plugins/uniform/jquery.uniform.min.js'
                    );
                    $page_level_plugin = array(
                        'assets/plugins/select2/select2.min.js',
                        'assets/plugins/data-tables/jquery.dataTables.js',
                        'assets/plugins/data-tables/DT_bootstrap.js',
                        'assets/plugins/jquery-file-upload/js/jquery.fileupload.js',
                        'assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                        'assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                        'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                        'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js',
                        'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js',
                        'assets/plugins/bootstrap-paginator-master/src/bootstrap-paginator.js'
                    );
                    $page_level_script = array(
                        'assets/js/app.js',
                        'assets/js/modaldata.js',
                        'assets/js/form-components.js'
                    );
                    $inline_js = " <script>
									  jQuery(document).ready(function() {    
										 // initiate layout and plugins
										 App.init();
										 FormComponents.init();
									  });
								   </script>";
                    break;
                case "list" :
                    $plugin = array(
                        'assets/plugins/fontawesome/css/font-awesome.min.css',
                        'assets/plugins/bootstrap/css/bootstrap.css',
                        'assets/plugins/uniform/css/uniform.default.css',
                        'assets/plugins/select2/select2_metro.css',
                        'assets/plugins/data-tables/DT_bootstrap.css',
                        'assets/plugins/bootstrap-datepicker/css/datepicker.css',
                        'assets/plugins/bootstrap-timepicker/compiled/timepicker.css',
                        'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css'
                    );
                    $theme = array(
                        'assets/css/style-metronic.css',
                        'assets/css/style.css',
                        'assets/css/style-responsive.css',
                        'assets/css/plugins.css',
                        'assets/css/light.css',
                        'assets/css/custom.css'
                    );
                    $javascript = array(
                        'assets/js/jquery-migrate-1.2.1.min.js',
                        'assets/plugins/bootstrap/js/bootstrap.min.js',
                        'assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js',
                        'assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
                        'assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
                        'assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js',
                        'assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js',
                        'assets/js/jquery.blockui.min.js',
                        'assets/js/jquery.cookie.min.js',
                        'assets/plugins/uniform/jquery.uniform.min.js'
                    );
                    $page_level_plugin = array(
                        'assets/plugins/select2/select2.min.js',
                        'assets/plugins/data-tables/jquery.dataTables.js',
                        'assets/plugins/data-tables/DT_bootstrap.js',
                        'assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                        'assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js'
                    );
                    $page_level_script = array(
                        'assets/js/app.js',
                        'assets/js/form-components.js',
                        'assets/js/table-advanced.js'
                    );
                    $inline_js = " <script>
									  jQuery(document).ready(function() {    
										 // initiate layout and plugins
										$('#deleteModal').on('show.bs.modal', function(e) {
											$(this).find('.danger').attr('onclick', 'location.href=\"' + $(e.relatedTarget).data('href') + '\"');
										});
										$('#del_All').on('show.bs.modal', function (e) {
											$(this).find('.danger2').attr('onclick', 'go_delete(\"' + $(e.relatedTarget).data('href') + '\")');
										});
										 App.init();
										 FormComponents.init();
                                         TableAdvanced.init();
									  });
									  function go_delete(p_url) {
											id_obj = [];
											$('.checkboxes:checked').each(function ()
											{
												ids = $(this).data();
												var id_array = {};
												$.each(ids,function(index, value){
													if(index !== 'uniformed'){
														id_array[index] = value;
													}
												});
												id_obj.push(id_array);
											}).get();
											var post = {ids: id_obj, updated_by: '" . $this->current_user->user_username . "', updated_on:'" . date('Y-m-d H:i:s') . "'};
											$.ajax({
												url: p_url,
												type: 'post',
												dataType: 'json',
												data: JSON.stringify(post),
												success: function (data) {
													console.log(data);
													location.reload();
												}
											});
											$('#del_All').modal('hide');
										}
								   </script>";
                    break;
            }
            //menu
            $menu_result = $this->ci->m_menu->get_active_menu($this->current_user->role_id, null);
			$i = 0;
            foreach ($menu_result as $menu) {
                $this->obj_id = $menu->menu_id;
                $child = $this->ci->m_menu->get_active_menu($this->current_user->role_id, $this->obj_id);
				$menu_result[$i]->submenu = $child;
                $j = 0;
                foreach ($child as $submenu) {
                    $grandchild = $this->ci->m_menu->get_active_menu($this->current_user->role_id, $submenu->menu_id);
                    $menu_result[$i]->submenu[$j]->subsubmenu = $grandchild;
                    $j++;
                }
                $i++;
            }
            $data['plugins'] = $plugin;
            $data['theme'] = $theme;
            $data['basic_js'] = $basic_js;
            $data['javascript'] = $javascript;
            $data['page_level_plugin'] = $page_level_plugin;
            $data['page_level_script'] = $page_level_script;
            $data['inline_js'] = $inline_js;
            $data['content_menu'] = (!empty($menu_result)) ? $menu_result : array();
            $data['login_user'] = $this->current_user;
            $data['current_menu'] = $current_menu;
			$data['message'] = $this->message;
            $data['_content'] = $this->ci->load->view('admin/' . $template, $data, true);

            $this->ci->load->view('/admin/template.php', $data);
        } else {
            redirect(base_url() . 'admin/login');
        }
    }

}

?>