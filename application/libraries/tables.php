<?php
	class Tables{
		PUBLIC STATIC $activity = "activity";
		PUBLIC STATIC $activity_log = "activity_log";
		PUBLIC STATIC $book_category = "book_category";
		PUBLIC STATIC $books = "books";
		PUBLIC STATIC $file_download = "file_download";
		PUBLIC STATIC $image_slideshow = "image_slideshow";
		PUBLIC STATIC $link_internal = "link_internal";
		PUBLIC STATIC $menu = "menu";
		PUBLIC STATIC $menu_highlight = "menu_highlight";
		PUBLIC STATIC $menu_role = "menu_role";
		PUBLIC STATIC $messages = "messages";
		PUBLIC STATIC $news = "news";
		PUBLIC STATIC $news_category = "news_category";
		PUBLIC STATIC $news_comment = "news_comment";
		PUBLIC STATIC $photo_gallery = "photo_gallery";
		PUBLIC STATIC $role = "role";
		PUBLIC STATIC $static_content = "static_content";
		PUBLIC STATIC $struktur_organisasi = "struktur_organisasi";
		PUBLIC STATIC $subscribers = "subscribers";
		PUBLIC STATIC $testimonial = "testimonial";
		PUBLIC STATIC $users = "users";
		PUBLIC STATIC $website_info = "website_info";
		PUBLIC STATIC $tv = "tv";
		PUBLIC STATIC $video = "video";
		PUBLIC STATIC $layanan = "layanan";
		PUBLIC STATIC $fb_user = "fb_user";
		PUBLIC STATIC $forum_discussion = "forum_discussion";
		PUBLIC STATIC $forum_topic = "forum_topic";
		PUBLIC STATIC $polling = "polling";
		PUBLIC STATIC $sub_polling = "sub_polling";
		PUBLIC STATIC $bos = "bos";
		PUBLIC STATIC $olimpiade = "olimpiade";
		PUBLIC STATIC $pembelajaran = "pembelajaran";
		PUBLIC STATIC $sarana = "sarana";
		PUBLIC STATIC $sambutan = "sambutan";
	}
 ?>