<?php
class Template {

    function __construct() {
        $this->ci = &get_instance();
        init_generic_dao();
        $this->ci->load->model(array('m_link_internal','m_website_info','m_static_content','m_news_category'));
    }

    function display($template, $data = null, $berita = null) {
        if(!empty($berita)){
            $data['id'] = $data['berita']->news_id;
            $data['image'] = $data['berita']->news_photo;
            $data['title'] = $data['berita']->news_title;
            $data['description'] = substr($data['berita']->news_content, 0, 100)."...";
        }
        $data['link_internal'] = $this->ci->m_link_internal->fetch();
        $data['web_info'] = $this->ci->m_website_info->by_id(array('id'=>1));
        $data['static_content'] = $this->ci->m_static_content->fetch(10,0,'static_id',true,array('static_iscontactinfo'=>0,'static_status'=>1));
        $data['news_category'] = $this->ci->m_news_category->fetch(10,0,'category_id',true,array('category_status'=>1));
        $data['_content'] = $this->ci->load->view($template, $data, true);
        $this->ci->load->view('/template.php', $data);
    }

}
?>